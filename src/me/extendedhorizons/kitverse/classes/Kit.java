package me.extendedhorizons.kitverse.classes;

import com.mojang.authlib.GameProfile;
import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.tier1.*;
import me.extendedhorizons.kitverse.classes.tier2.*;
import me.extendedhorizons.kitverse.classes.tier3.*;
import me.extendedhorizons.kitverse.util.ArmorSet;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Kit {
    Main main = Main.getInstance();
    public List<Kit> kitList = new ArrayList<>();
    public HashMap<Player, GameProfile> profiles = new HashMap<>();

    Player owner;
    EntityPlayer npc;
    String name;
    World world;
    Location location;
    ItemStack heldItem;
    ArmorSet armor;
    GameProfile profile;
    int tierNumber;
    byte yaw = 0;
    byte pitch = 0;
    byte headRotation = 0;

    public void loadSpawnKits(Player player) {
        new Archer(player, new Location(world,52.500,13,49.500));
        new Brewer(player, new Location(world,54.500,13,49.500));
        new Thief(player, new Location(world,56.500,13,50.500));
        new Warrior(player, new Location(world,50.500,13,49.500));
        new Mage(player, new Location(world,48.500,13,50.500));

        new Marksman(player, new Location(world,44.500,13,57.500));
        new Chemist(player, new Location(world,44.500,13,55.500));
        new Rogue(player, new Location(world,45.500,13,53.500));
        new Knight(player, new Location(world,44.500,13,59.500));
        new Warlock(player, new Location(world,45.500,13,61.500));

        new Ranger(player, new Location(world,60.500,13,57.500));
        new Alchemist(player, new Location(world,60.500,13,55.500));
        new Assassin(player, new Location(world,59.500,13,53.500));
        new Warlord(player, new Location(world,60.500,13,59.500));
        new Sorcerer(player, new Location(world,59.500,13,61.500));

        main.getUser(player.getUniqueId()).resetPlayerName(player);
    }

    public void clearKits(Player player) {
        for (Kit kits : kitList) {
            if (kits.getOwner().equals(player)) {
                PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                try {
                    connection.sendPacket(new PacketPlayOutEntityDestroy(kits.getNpc().getBukkitEntity().getEntityId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void clearAllKits(Player player) {
        for (Kit kits : kitList) {
            PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
            try {
                connection.sendPacket(new PacketPlayOutEntityDestroy(kits.getNpc().getBukkitEntity().getEntityId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadKit(Kit kit, Player player) {

        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) kit.getWorld()).getHandle();

        PlayerInteractManager pim = new PlayerInteractManager(nmsWorld);
        EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, profile, pim);
        npc.setLocation(kit.getLocation().getX(), kit.getLocation().getY(), kit.getLocation().getZ(), kit.getYaw(), kit.getPitch());
        npc.displayName = player.getName();
        npc.spawnIn(nmsWorld);

        this.npc = npc;
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

        try {
            connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getBukkitEntity().getEntityId(), EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(kit.getArmor().getHelmet())));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getBukkitEntity().getEntityId(), EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(kit.getArmor().getChestplate())));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getBukkitEntity().getEntityId(), EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(kit.getArmor().getLeggings())));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getBukkitEntity().getEntityId(), EnumItemSlot.FEET, CraftItemStack.asNMSCopy(kit.getArmor().getBoots())));
            connection.sendPacket(new PacketPlayOutEntityEquipment(npc.getBukkitEntity().getEntityId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(kit.getHeldItem())));
            connection.sendPacket(new PacketPlayOutEntity.PacketPlayOutEntityLook(kit.getNpc().getBukkitEntity().getEntityId(), kit.getYaw(), kit.getPitch(), true));
            connection.sendPacket(new PacketPlayOutEntityHeadRotation(npc, kit.getHeadRotation()));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setHeldItem(ItemStack heldItem) {
        this.heldItem = heldItem;
    }

    public void setArmor(ArmorSet armor) {
        this.armor = armor;
    }

    public void setYaw(byte yaw) {
        this.yaw = yaw;
    }

    public void setPitch(byte pitch) {
        this.pitch = pitch;
    }

    public void setOwner(Player player) {
        this.owner = player;
    }

    public void setGameProfile(Player player, Kit kit) {
        this.profile = new GameProfile(player.getUniqueId(), kit.getName());
    }

    public void setTierNumber(int i) {
        this.tierNumber = i;
    }

    public void setHeadRotation(byte headRotation) {
        this.headRotation = headRotation;
    }

    //Getters
    public String getName() {
        return name;
    }

    public World getWorld() {
        return world;
    }

    public Location getLocation() {
        return location;
    }

    public ItemStack getHeldItem() {
        return heldItem;
    }

    public ArmorSet getArmor() {
        return armor;
    }

    public EntityPlayer getNpc() {
        return npc;
    }

    public Player getOwner() {
        return owner;
    }

    public byte getYaw() {
        return yaw;
    }

    public byte getPitch() {
        return pitch;
    }

    public GameProfile getGameProfile() {
        return profile;
    }

    public byte getHeadRotation() {
        return headRotation;
    }

    public int getTierNumber() {
        return this.tierNumber;
    }

}
