package me.extendedhorizons.kitverse.classes;

import me.extendedhorizons.kitverse.util.Locations;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by horthrax on 8/29/2015.
 */
public class Selectors {
    World world = Bukkit.getWorld("world");
    Location archerLoc = new Location(world, 52.500, 13, 49.750);
    Location brewerLoc = new Location(world, 54.500, 13, 49.750);
    Location warriorLoc = new Location(world, 50.500, 13, 49.750);
    Location thiefLoc = new Location(world, 56.250, 13, 50.750, 35, 0);
    Location mageLoc = new Location(world, 48.750, 13, 50.750, -35, 0);

    Location chemistLoc = new Location(world, 44.750, 13, 55.500, -90, 0);
    Location knightLoc = new Location(world, 44.750, 13, 59.500, -90, 0);
    Location marksmanLoc = new Location(world, 44.750, 13, 57.500, -90, 0);
    Location rogueLoc = new Location(world, 45.750, 13, 53.750, -55, 0);
    Location warlockLoc = new Location(world, 45.750, 13, 61.250, 55, 0);

    Location alchemistLoc = new Location(world, 60.250, 13, 55.500, 90, 0);
    Location warlordLoc = new Location(world, 60.250, 13, 59.500, 90, 0);
    Location rangerLoc = new Location(world, 60.250, 13, 57.500, 90, 0);
    Location assassinLoc = new Location(world, 59.250, 13, 53.750, 55, 0);
    Location sorcererLoc = new Location(world, 59.250, 13, 61.250, -55, 0);

    Location spawnBlacksmithLoc = new Location(world, 52.5, 13, 66.300);
    Location spawnTutorialLoc = new Location(world, 53.9, 13, 61.5);

    public ArrayList<ArmorStand> selectors = new ArrayList<>();
    public HashMap<ArmorStand, String> selectorKit = new HashMap<>();

    public Selectors(){

    }
    public void clearSelectors(){
        for(ArmorStand selectorStands : selectors){
            selectorStands.remove();
        }
    }
    public void loadAllSelectors() {
        //Tier 1
        new SelectorObject(archerLoc, "Archer");
        new SelectorObject(brewerLoc, "Brewer");
        new SelectorObject(warriorLoc, "Warrior");
        new SelectorObject(thiefLoc, "Thief");
        new SelectorObject(mageLoc, "Mage");
        //Tier 2
        new SelectorObject(chemistLoc, "Chemist");
        new SelectorObject(knightLoc, "Knight");
        new SelectorObject(marksmanLoc, "Marksman");
        new SelectorObject(rogueLoc, "Rogue");
        new SelectorObject(warlockLoc, "Warlock");
        //Tier 3
        new SelectorObject(alchemistLoc, "Alchemist");
        new SelectorObject(warlordLoc, "Warlord");
        new SelectorObject(rangerLoc, "Ranger");
        new SelectorObject(assassinLoc, "Assassin");
        new SelectorObject(sorcererLoc, "Sorcerer");
        //Blacksmiths
        new SelectorObject(spawnBlacksmithLoc, "Blacksmith");
        new SelectorObject(spawnTutorialLoc, "Tutorial");
    }
    public void addSelector(ArmorStand armorStand){
        this.selectors.add(armorStand);
    }
    public void addSelectorKit(ArmorStand stand, String kit){
        this.selectorKit.put(stand,kit);
    }
}
