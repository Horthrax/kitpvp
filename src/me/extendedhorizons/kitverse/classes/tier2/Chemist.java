package me.extendedhorizons.kitverse.classes.tier2;

import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.Kit;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Chemist extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Chemist (Player player, Location loc){
        //Location loc = new Location(world,44.500,96,55.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.GOLD_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Chemist's Helmet");
        helmet.setItemMeta(meta4);


        ItemStack chestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Chemist's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Chemist's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Chemist's Boots");
        boots.setItemMeta(meta2);

        ArmorSet chemistSet = new ArmorSet();
        chemistSet.setHelmet(helmet);
        chemistSet.setChestplate(chestplate);
        chemistSet.setLeggings(leggings);
        chemistSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.WOOD_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Hard Oak Sword");
        held.setItemMeta(heldMeta);

        ItemStack brewingStand = new ItemStack(Material.BREWING_STAND_ITEM,1);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.LIGHT_PURPLE + "Chemist");
        this.setTierNumber(2);
        this.setArmor(chemistSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) -90);
        this.setHeadRotation((byte) -65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(chemistSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(chemistSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(chemistSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(chemistSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(brewingStand,1);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("chemist",loadoutItems);
    }
}
