package me.extendedhorizons.kitverse.classes.tier2;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Marksman extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Marksman(Player player, Location loc){
        //Location loc = new Location(world,44.500,96,57.500);
        //ARMORSET
        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY + "Marksman's Chestplate");
        meta.setColor(Color.GRAY);
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.GRAY + "Marksman's Leggings");
        meta1.setColor(Color.SILVER);
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.GRAY + "Marksman's Boots");
        boots.setItemMeta(meta2);

        ArmorSet marksmanSet = new ArmorSet();
        marksmanSet.setHelmet(air);
        marksmanSet.setChestplate(chestplate);
        marksmanSet.setLeggings(leggings);
        marksmanSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.BOW);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Cross Bow");
        held.setItemMeta(heldMeta);

        ItemStack arrows = new ItemStack(Material.ARROW,64);
        ItemStack sword = new ItemStack(Material.WOOD_SWORD);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GRAY + "Marksman");
        this.setTierNumber(2);
        this.setArmor(marksmanSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) -90);
        this.setHeadRotation((byte) -65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(marksmanSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(marksmanSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(marksmanSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(marksmanSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(sword,0);
        loadoutItems.put(held,1);
        loadoutItems.put(arrows,8);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("marksman",loadoutItems);
    }
}
