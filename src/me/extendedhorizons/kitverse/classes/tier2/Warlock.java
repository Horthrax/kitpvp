package me.extendedhorizons.kitverse.classes.tier2;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Warlock extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Warlock(Player player, Location loc){
        //Location loc = new Location(world,45.500,96,61.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
        LeatherArmorMeta meta4 = (LeatherArmorMeta) helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Warlock's Helmet");
        helmet.setItemMeta(meta4);


        ItemStack chestplate = new ItemStack(Material.GOLD_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Warlock's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.GOLD_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Warlock's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta meta2 = (LeatherArmorMeta) boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Warlock's Boots");
        boots.setItemMeta(meta2);

        ArmorSet warlockSet = new ArmorSet();
        warlockSet.setHelmet(helmet);
        warlockSet.setChestplate(chestplate);
        warlockSet.setLeggings(leggings);
        warlockSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.STICK);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Wand");
        heldMeta.addEnchant(Enchantment.DAMAGE_ALL,3,true);
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GOLD + "Warlock");
        this.setTierNumber(2);
        this.setArmor(warlockSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setYaw((byte) -115);
        this.setHeadRotation((byte) -90);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(warlockSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(warlockSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(warlockSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(warlockSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("warlock",loadoutItems);
    }
}
