package me.extendedhorizons.kitverse.classes.tier2;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Knight extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Knight (Player player, Location loc){
        //Location loc = new Location(world,44.500,96,59.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.CHAINMAIL_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.DARK_GRAY + "Knight's Helmet");
        helmet.setItemMeta(meta4);

        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "Knight's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.DARK_GRAY + "Knight's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.DARK_GRAY + "Knight's Boots");
        boots.setItemMeta(meta2);

        ArmorSet knightSet = new ArmorSet();
        knightSet.setHelmet(helmet);
        knightSet.setChestplate(chestplate);
        knightSet.setLeggings(leggings);
        knightSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.STONE_AXE);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Broad Axe");
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.DARK_RED + "Knight");
        this.setTierNumber(2);
        this.setArmor(knightSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) -90);
        this.setHeadRotation((byte) -65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(knightSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(knightSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(knightSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(knightSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("knight",loadoutItems);
    }
}
