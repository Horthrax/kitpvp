package me.extendedhorizons.kitverse.classes.tier2;

import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.Kit;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Rogue extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Rogue(Player player, Location loc){
        //Location loc = new Location(world,45.500,96,53.500);
        //ARMORSET

        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
        meta.setColor(Color.SILVER);
        meta.setDisplayName(ChatColor.RED + "Rogue's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setColor(Color.SILVER);
        meta1.setDisplayName(ChatColor.RED + "Rogue's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.RED + "Rogue's Boots");
        boots.setItemMeta(meta2);

        ArmorSet rogueSet = new ArmorSet();
        rogueSet.setHelmet(air);
        rogueSet.setChestplate(chestplate);
        rogueSet.setLeggings(leggings);
        rogueSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.WOOD_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Wooden Dagger");
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.RED + "Rogue");
        this.setTierNumber(2);
        this.setArmor(rogueSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setYaw((byte) -65);
        this.setHeadRotation((byte) -40);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(rogueSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(rogueSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(rogueSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(rogueSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("rogue",loadoutItems);
    }
}
