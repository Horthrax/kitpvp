package me.extendedhorizons.kitverse.classes;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;

import java.util.UUID;

/**
 * Created by horthrax on 7/1/2015.
 */
public enum Tier {
    //ARCHER_TIER,WARRIOR_TIER,BREWER_TIER,THIEF_TIER,MAGE_TIER;
    ARCHER_TIER("archer_tier","Archer", "Marksman", "Ranger"),
    WARRIOR_TIER("warrior_tier","Warrior", "Knight", "Warlord"),
    BREWER_TIER("brewer_tier","Brewer", "Chemist", "Alchemist"),
    THIEF_TIER("thief_tier","Thief", "Rogue", "Assassin"),
    MAGE_TIER("mage_tier","Mage", "Warlock", "Sorcerer");

    private String name, tier1, tier2, tier3;

    private Tier(String name, String tier1, String tier2, String tier3){
        this.name = name;
        this.tier1 = tier1;
        this.tier2 = tier2;
        this.tier3 = tier3;
    }

    public String getName(){
        return this.name;
    }

    public String getTier1(){
        return this.tier1;
    }
    public String getTier2(){
        return this.tier2;
    }
    public String getTier3(){
        return this.tier3;
    }

    public static boolean isValid(String tier){
        for(Tier t : values()){
            if(t.getName().equalsIgnoreCase(tier)){
                return true;
            }
        }
        return false;
    }

    public static Tier getTierFromString(String tier){
        for(Tier t : values()){
            if(t.getName().equalsIgnoreCase(tier)){
                return t;
            }
        }
        return null;
    }

    public static Tier getTierFromKit(String kit){
        for(Tier t : values()){
            if(t.getTier1().equalsIgnoreCase(kit)){
                return t;
            }
            if(t.getTier2().equalsIgnoreCase(kit)){
                return t;
            }
            if(t.getTier3().equalsIgnoreCase(kit)){
                return t;
            }
        }
        return null;
    }

    public static int getTierLevel(Tier tierType, String className){
        int level;
        if(tierType.getTier1().equalsIgnoreCase(className)){
            level = 1;
        }else if(tierType.getTier2().equalsIgnoreCase(className)){
            level = 2;
        }else if(tierType.getTier3().equalsIgnoreCase(className)){
            level = 3;
        }else{
            level = 0;
        }
        return level;
    }

    public static Tier getTierType(String className){
        for(Tier t : Tier.values()){
            if(t.getTier1().equalsIgnoreCase(className) || t.getTier2().equalsIgnoreCase(className) || t.getTier3().equalsIgnoreCase(className)){
                return t;
            }
        }
        return null;
    }
}
