package me.extendedhorizons.kitverse.classes.abilities;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.inventories.BrewerGui;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import me.extendedhorizons.kitverse.util.RGB;
import me.extendedhorizons.kitverse.util.ReflectionUtil;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.event.block.Action;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 10/22/2015.
 */
@SuppressWarnings("unused")
public class Ability {
    Main main = Main.getInstance();
    ReflectionUtil reflectionUtil = new ReflectionUtil();
    BrewerGui brewerGui = new BrewerGui();

    public Ability(Action action, Player player, String tierName, int tierNumber) {
        if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (tierName.equals("archer_tier")) {
                archerAbility(player, tierNumber);
            }
            if (tierName.equals("mage_tier")) {
                mageAbility(player, tierNumber);
            }
            if (tierName.equals("warrior_tier")) {
                warriorAbility(player, tierNumber);
            }
            if (tierName.equals("thief_tier")) {
                thiefAbility(player, tierNumber);
            }
            if(tierName.equals("brewer_tier")){
                brewerAbility(player, tierNumber);
            }
        }
    }

    public void archerAbility(Player player, int tierNumber) {
        int cooldown = 0;
        int arrows = 0;
        long delay = 0;
        double distance = 0;
        switch (tierNumber) {
            case (1):
                cooldown = 60;
                arrows = 3;
                delay = 6;
                distance = 1.4;
                break;
            case (2):
                cooldown = 50;
                arrows = 4;
                delay = 4;
                distance = 1.6;
                break;
            case (3):
                cooldown = 35;
                arrows = 6;
                delay = 2;
                distance = 2;
                break;
        }

        final int arrowCount = arrows;
        final double arrowDistance = distance;
        Material itemType = player.getItemInHand().getType();
        if (itemType.equals(Material.BOW)) {
            main.getUser(player.getUniqueId()).setAbilityCooldown(cooldown * 20);
            main.getUser(player.getUniqueId()).setPlayerState(PlayerState.ABILITY);
            new BukkitRunnable() {
                int i = 0;

                @Override
                public void run() {
                    i++;
                    if (i <= arrowCount) {
                        Arrow arrow = player.launchProjectile(Arrow.class);
                        arrow.setShooter(player);
                        arrow.setVelocity(player.getLocation().getDirection().multiply(arrowDistance));
                    } else {
                        main.getUser(player.getUniqueId()).setPlayerState(PlayerState.PVP);
                        this.cancel();
                    }
                }
            }.runTaskTimer(main, 1, delay);
        }
    }

    public void warriorAbility(Player player, int tierNumber) {
        int cooldown = 0;

        double heathDecrease = 0;
        int resistance = 0;
        int resistanceTime = 0;
        int strength = 0;
        int strengthTime = 0;
        int weaknessTime = 0;

        switch (tierNumber) {
            case (1):
                cooldown = 60;
                heathDecrease = 14;
                resistance = 0;
                resistanceTime = 7;
                strength = 0;
                strengthTime = 4;
                weaknessTime = 15;
                break;
            case (2):
                cooldown = 50;
                heathDecrease = 16;
                resistance = 1;
                resistanceTime = 10;
                strength = 1;
                strengthTime = 6;
                weaknessTime = 13;
                break;
            case (3):
                cooldown = 35;
                heathDecrease = 18;
                resistance = 2;
                resistanceTime = 13;
                strength = 2;
                strengthTime = 8;
                weaknessTime = 10;
                break;
        }
        Material itemType = player.getItemInHand().getType();
        if (itemType.equals(Material.WOOD_AXE) || itemType.equals(Material.STONE_AXE) || itemType.equals(Material.IRON_AXE)) {
            main.getUser(player.getUniqueId()).setAbilityCooldown(cooldown * 20);
            main.getUser(player.getUniqueId()).setPlayerState(PlayerState.ABILITY);
            player.setHealthScale(heathDecrease);
            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * resistanceTime, resistance, false, true));
            //player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * strengthTime, strength, false, true));
            player.getWorld().playSound(player.getLocation(), Sound.BLOCK_METAL_BREAK, 0.5F, 1);
            new BukkitRunnable() {
                @Override
                public void run() {
                    main.getUser(player.getUniqueId()).setPlayerState(PlayerState.PVP);
                    player.setHealthScale(20);
                }
            }.runTaskLater(main, 20 * weaknessTime);
        }
    }

    public void thiefAbility(Player player, int tierNumber) {
        int cooldown = 0;
        int speed = 0;
        int speedTime = 0;
        int weaknessStabTime = 0;

        switch (tierNumber) {
            case (1):
                cooldown = 60;
                speed = 0;
                speedTime = 10;
                weaknessStabTime = 10;
                break;
            case (2):
                cooldown = 50;
                speed = 1;
                speedTime = 15;
                weaknessStabTime = 15;
                break;
            case (3):
                cooldown = 35;
                speed = 2;
                speedTime = 22;
                weaknessStabTime = 20;
                break;
        }
        Material itemType = player.getItemInHand().getType();
        if (itemType.equals(Material.STONE_SWORD) || itemType.equals(Material.WOOD_SWORD)) {
            main.getUser(player.getUniqueId()).setPlayerState(PlayerState.ABILITY);
            main.getUser(player.getUniqueId()).setAbilityCooldown(cooldown * 20);
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * speedTime, speed, false, true));
            player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_FLAP, 0.5F, 1);
            new BukkitRunnable() {
                @Override
                public void run() {
                    main.getUser(player.getUniqueId()).setPlayerState(PlayerState.PVP);
                }
            }.runTaskLater(main, weaknessStabTime * 20);
        }
    }

    public void mageAbility(Player player, int tierNumber) {
        int cooldown = 0;
        int orbLifeSpan = 0;
        int orbDamage = 0;
        int orbAmount = 0;
        int orbSize = 0;
        int delay = 0;
        RGB orbColor1 = new RGB(255, 100, 0);//255,119,0
        RGB orbColor2 = new RGB(255, 150, 0);//255,196,95
        RGB orbColor3 = new RGB(255, 200, 0);//238,255,125

        switch (tierNumber) {
            case (1):
                cooldown = 60; //originally 45
                orbLifeSpan = 15;
                orbDamage = 4;
                orbAmount = 1;
                orbSize = 2;
                delay = 1;
                break;
            case (2):
                cooldown = 50;
                orbLifeSpan = 18;
                orbDamage = 4;
                orbAmount = 2;
                orbSize = 6;
                delay = 13;
                break;
            case (3):
                cooldown = 35;
                orbLifeSpan = 22;
                orbDamage = 4;
                orbAmount = 3;
                orbSize = 10;
                delay = 10;
                break;
        }
        RGB[] colors = new RGB[3];
        colors[0] = orbColor1;
        colors[1] = orbColor2;
        colors[2] = orbColor3;
        Material itemType = player.getItemInHand().getType();
        if (itemType.equals(Material.STICK)) {
            main.getUser(player.getUniqueId()).setPlayerState(PlayerState.ABILITY);
            main.getUser(player.getUniqueId()).setAbilityCooldown(cooldown * 20);
            final int amountOfOrbs = orbAmount;
            final int orbParticleSize = orbSize;
            final int orbParticleDistance = orbLifeSpan;
            final int orbParticleDamage = orbDamage;
            final RGB[] orbParticleColors = colors;
            World world = player.getWorld();
            new BukkitRunnable() {
                int i = 0;
                @Override
                public void run() {
                    if (i < amountOfOrbs) {
                        i++;
                        world.playSound(player.getLocation(), Sound.ENTITY_GHAST_SHOOT, 0.5F, 1);
                        spawnOrbs(player,orbParticleSize, i, orbParticleDamage, orbParticleColors, orbParticleDistance, 0.3);
                    } else {
                        this.cancel();
                    }
                }
            }.runTaskTimer(main, 0, delay);
        }
    }
    public boolean isEntityNearby(Entity entity, Entity nearby, double x, double y, double z){
        for(org.bukkit.entity.Entity near : entity.getNearbyEntities(x, y, z)){
            if(near.equals(nearby)){
                return true;
            }
        }
        return false;
    }
    public boolean isEntityTypeNearby(Entity entity, EntityType type, double x, double y, double z){
        for(org.bukkit.entity.Entity near : entity.getNearbyEntities(x, y, z)){
            if(near.getType().equals(type)){
                return true;
            }
        }
        return false;
    }
    public List<Entity> getNearbyEntityTypeExclude(Entity entity, EntityType type, Entity exclude, double x, double y, double z){
        List<Entity> entities = new ArrayList<>();
        for(Entity near : entity.getNearbyEntities(x, y, z)){
            if(near.getType().equals(type) && near != exclude){
                entities.add(near);
            }
        }
        return entities;
    }
    public void spawnOrbs(Player player, int orbSize, int orbCount, int orbDamage, RGB[] orbColors, double totalDist, double distance){
        final Vector v = (player.getLocation().getDirection()).multiply(0.5);
        final World world = player.getWorld();
        double steps = totalDist / distance;
        Location loc = player.getEyeLocation();
        ArmorStand orbStand = (ArmorStand) world.spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);
        orbStand.setSmall(true);
        orbStand.setGravity(false);
        orbStand.setVisible(false);
        /*ItemStack ghastTear = new ItemStack(Material.GHAST_TEAR,1);
        orbStand.setItemInHand(ghastTear);
        for(Player onlinePlayers : Bukkit.getOnlinePlayers()){
            PlayerConnection playerConnection = ((CraftPlayer) onlinePlayers).getHandle().playerConnection;
            PacketPlayOutEntityEquipment equip = new PacketPlayOutEntityEquipment(orbStand.getEntityId(), 4, CraftItemStack.asNMSCopy(ghastTear));
            playerConnection.sendPacket(equip);
        }*/
        new BukkitRunnable() {
            int i = 0;
            @Override
            public void run() {
                if (i < steps) {
                    i++;
                    Location nextLoc = loc.add(v);
                    if(!(i >= steps)) {
                        if (nextLoc.getBlock().getType().equals(Material.AIR) || nextLoc.getBlock().getType().equals(Material.LADDER) || nextLoc.getBlock().getType().equals(Material.GRASS)) {
                            for(Entity entity : getNearbyEntityTypeExclude(orbStand, EntityType.PLAYER, player, 0.5, 1, 0.5)){
                                if(entity instanceof Player){
                                    Player damagedPlayer = (Player) entity;
                                    damagedPlayer.damage(orbDamage);
                                }
                            }
                            if (getNearbyEntityTypeExclude(orbStand, EntityType.PLAYER, player, 0.2, 0.2, 0.2).isEmpty()) {
                                orbStand.teleport(nextLoc);
                                spawnColoredParticle(nextLoc, EnumParticle.REDSTONE, true, orbColors[orbCount - 1], orbSize);
                            } else {
                                destroyOrb(orbStand, nextLoc, v);
                                this.cancel();
                            }
                        } else {
                            destroyOrb(orbStand, nextLoc, v);
                            this.cancel();
                        }
                    } else {
                        destroyOrb(orbStand, nextLoc, v);
                        this.cancel();
                    }
                } else {
                    orbStand.remove();
                    this.cancel();
                }
            }
         }.runTaskTimer(main, 0, 1);
    }
    public void destroyOrb(ArmorStand orbStand, Location nextLoc, Vector v){
        orbStand.remove();
        Location oldLoc = nextLoc.subtract(v);
        float oldX = (float) oldLoc.getX();
        float oldY = (float) (oldLoc.getY() - 0.3);
        float oldZ = (float) oldLoc.getZ();
        PacketPlayOutWorldParticles explodePacket = new PacketPlayOutWorldParticles(EnumParticle.SMOKE_NORMAL, true, oldX, oldY, oldZ, 0, 0, 0, 0.1F, 50, null);
        for (Player players : Bukkit.getOnlinePlayers()) {
            PlayerConnection playerConnection = ((CraftPlayer) players).getHandle().playerConnection;
            playerConnection.sendPacket(explodePacket);
        }
        nextLoc.getWorld().playSound(oldLoc, Sound.ITEM_FLINTANDSTEEL_USE, 0.5F, 1);
    }

    public void spawnColoredParticle(Location location, EnumParticle particle, boolean longRange, RGB rgb, int particleSize){
        //Constructor constructor = reflectionUtil.getConstructor(PacketPlayOutWorldParticles.class);
        PacketPlayOutWorldParticles particlesPacket = new PacketPlayOutWorldParticles(particle,longRange,(float)location.getX(),(float)(location.getY() - 0.5), (float)location.getZ(), rgb.getRedFloat(), rgb.getGreenFloat(), rgb.getBlueFloat(),1,0,null);
        for(int i = 0; i < particleSize; i++) {
            for(Player player : Bukkit.getOnlinePlayers()){
                PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
                try {
                    playerConnection.sendPacket(particlesPacket);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
    public void spawnParticle(Location location, EnumParticle particle, boolean longRange, int xOffs, int yOffs, int zOffs, int speed, int size){
        PacketPlayOutWorldParticles particlesPacket = new PacketPlayOutWorldParticles(particle,longRange,(float)location.getX(),(float)(location.getY()), (float)location.getZ(), xOffs,yOffs,zOffs,speed,size);
        for(Player players : Bukkit.getOnlinePlayers()){
            PlayerConnection connection = ((CraftPlayer) players).getHandle().playerConnection;
            connection.sendPacket(particlesPacket);
        }
    }
    private void brewerAbility(Player player, int tierNumber) {
        Material itemType = player.getItemInHand().getType();
        if (itemType.equals(Material.BREWING_STAND_ITEM)) {
            brewerGui.openGui(player,tierNumber);
        }
    }
}