package me.extendedhorizons.kitverse.classes.abilities;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by horthrax on 10/22/2015.
 */
public class AbilityTriggers implements Listener {
    Main main = Main.getInstance();

    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        User user = main.getUser(player.getUniqueId());
        String selectedKit = user.getSelectedKit();
        Tier tier = Tier.getTierFromKit(selectedKit);
        if (selectedKit != null && tier != null) {
            if (!user.hasAbilityCooldown()) {
                if(player.isSneaking()) {
                    if (event.getItem() != null) {
                        new Ability(event.getAction(), player, tier.getName(), Tier.getTierLevel(tier, selectedKit));
                    }
                }
            }
        }
    }

    @EventHandler
    public void thiefWeaknessStab(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
            Player player = (Player) event.getDamager();
            Player damaged = (Player) event.getEntity();
            User user = main.getUser(player.getUniqueId());
            if(user.getPlayerState().equals(PlayerState.ABILITY)){
                Tier tier = Tier.getTierFromKit(user.getSelectedKit());
                if(tier != null && tier.equals(Tier.THIEF_TIER)){
                    int weaknessStab = 0;
                    int weaknessStabTime = 0;
                    switch(Tier.getTierLevel(tier,user.getSelectedKit())) {
                        case (1):
                            weaknessStab = 0;
                            weaknessStabTime = 10;
                            break;
                        case (2):
                            weaknessStab = 1;
                            weaknessStabTime = 15;
                            break;
                        case (3):
                            weaknessStab = 2;
                            weaknessStabTime = 20;
                            break;
                    }
                    damaged.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS,weaknessStabTime,weaknessStab));
                }
            }
        }
    }
}