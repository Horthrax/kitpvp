package me.extendedhorizons.kitverse.classes.tier3;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Assassin extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Assassin(Player player, Location loc){
        //Location loc = new Location(world,59.500,96,53.500);
        //ARMORSET

        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
        meta.setColor(Color.GRAY);
        meta.setDisplayName(ChatColor.RED + "Assassin's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setColor(Color.GRAY);
        meta1.setDisplayName(ChatColor.RED + "Assassin's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.RED + "Assassin's Boots");
        boots.setItemMeta(meta2);

        ArmorSet assassinSet = new ArmorSet();
        assassinSet.setHelmet(air);
        assassinSet.setChestplate(chestplate);
        assassinSet.setLeggings(leggings);
        assassinSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.STONE_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Stone Dagger");
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.RED + "Assassin");
        this.setTierNumber(3);
        this.setArmor(assassinSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setYaw((byte) 65);
        this.setHeadRotation((byte) 40);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(assassinSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(assassinSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(assassinSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(assassinSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("assassin",loadoutItems);
    }
}
