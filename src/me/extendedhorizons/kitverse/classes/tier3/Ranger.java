package me.extendedhorizons.kitverse.classes.tier3;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Ranger extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Ranger(Player player, Location loc){
        //Location loc = new Location(world,60.500,96,57.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
        LeatherArmorMeta meta3 = (LeatherArmorMeta) helmet.getItemMeta();
        meta3.setDisplayName(ChatColor.GRAY + "Ranger's Helmet");
        meta3.setColor(Color.GREEN);
        helmet.setItemMeta(meta3);

        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY + "Ranger's Chestplate");
        meta.setColor(Color.SILVER);
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.GRAY + "Ranger's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.GRAY + "Ranger's Boots");
        boots.setItemMeta(meta2);

        ArmorSet rangerSet = new ArmorSet();
        rangerSet.setHelmet(helmet);
        rangerSet.setChestplate(chestplate);
        rangerSet.setLeggings(leggings);
        rangerSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.BOW);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Long Bow");
        held.setItemMeta(heldMeta);

        ItemStack sword = new ItemStack(Material.STONE_SWORD);
        ItemMeta meta4 = sword.getItemMeta();
        meta4.setDisplayName(ChatColor.WHITE + "Sword");
        sword.setItemMeta(meta4);

        ItemStack arrows = new ItemStack(Material.ARROW,64);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GRAY + "Ranger");
        this.setTierNumber(3);
        this.setArmor(rangerSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) 90);
        this.setHeadRotation((byte) 65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(rangerSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(rangerSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(rangerSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(rangerSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(sword,0);
        loadoutItems.put(held,1);
        loadoutItems.put(arrows,8);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("ranger",loadoutItems);
    }
}
