package me.extendedhorizons.kitverse.classes.tier3;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Warlord extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Warlord (Player player, Location loc){
        //Location loc = new Location(world,60.500,96,59.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.IRON_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.DARK_GRAY + "Warlord's Helmet");
        helmet.setItemMeta(meta4);

        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "Warlord's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.DARK_GRAY + "Warlord's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.DARK_GRAY + "Knight's Boots");
        boots.setItemMeta(meta2);

        ArmorSet warlordSet = new ArmorSet();
        warlordSet.setHelmet(helmet);
        warlordSet.setChestplate(chestplate);
        warlordSet.setLeggings(leggings);
        warlordSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.IRON_AXE);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "War Axe");
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.DARK_RED + "Warlord");
        this.setTierNumber(3);
        this.setArmor(warlordSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) 90);
        this.setHeadRotation((byte) 65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(warlordSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(warlordSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(warlordSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(warlordSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("warlord",loadoutItems);
    }
}
