package me.extendedhorizons.kitverse.classes.tier3;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.util.ItemUtil;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Alchemist extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    ItemUtil itemUtil = new ItemUtil();
    public Alchemist (Player player, Location loc){
        //Location loc = new Location(world,60.500,96,55.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.CHAINMAIL_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Alchemist's Helmet");
        helmet.setItemMeta(meta4);


        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Alchemist's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Alchemist's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Alchemist's Boots");
        boots.setItemMeta(meta2);

        ArmorSet alchemistSet = new ArmorSet();
        alchemistSet.setHelmet(helmet);
        alchemistSet.setChestplate(chestplate);
        alchemistSet.setLeggings(leggings);
        alchemistSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.GOLD_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Infused Sword");
        heldMeta.addItemFlags();
        held.setItemMeta(heldMeta);

        ItemStack brewingStand = new ItemStack(Material.BREWING_STAND_ITEM,1);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.LIGHT_PURPLE + "Alchemist");
        this.setTierNumber(3);
        this.setArmor(alchemistSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) 90);
        this.setHeadRotation((byte) 65);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(alchemistSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(alchemistSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(alchemistSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(alchemistSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(brewingStand,1);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("alchemist",loadoutItems);
    }
}
