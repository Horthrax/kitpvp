package me.extendedhorizons.kitverse.classes.tier3;

import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.Kit;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Sorcerer extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Sorcerer(Player player, Location loc){
        //Location loc = new Location(world,59.500,96,61.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.GOLD_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Sorcerer's Helmet");
        helmet.setItemMeta(meta4);


        ItemStack chestplate = new ItemStack(Material.GOLD_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Sorcerer's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.GOLD_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Sorcerer's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Sorcerer's Boots");
        boots.setItemMeta(meta2);

        ArmorSet sorcererSet = new ArmorSet();
        sorcererSet.setHelmet(helmet);
        sorcererSet.setChestplate(chestplate);
        sorcererSet.setLeggings(leggings);
        sorcererSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.STICK);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Wand");
        heldMeta.addEnchant(Enchantment.DAMAGE_ALL,3,true);
        held.setItemMeta(heldMeta);

        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GOLD + "Sorcerer");
        this.setTierNumber(3);
        this.setArmor(sorcererSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setYaw((byte) 115);
        this.setHeadRotation((byte) 90);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(sorcererSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(sorcererSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(sorcererSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(sorcererSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("sorcerer",loadoutItems);
    }
}
