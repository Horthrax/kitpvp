package me.extendedhorizons.kitverse.classes.loadout;

import me.extendedhorizons.kitverse.util.ArmorSet;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by horthrax on 10/7/2015.
 */

public class Loadout extends ArmorSet{
    public static int headSlot = 39;
    public static int chestSlot = 38;
    public static int legSlot = 37;
    public static int bootSlot = 36;
    public static HashMap<String, HashMap<ItemStack, Integer>> loadouts = new HashMap<>();
    public static void applyLoadout(Player player, String name){
        String kitName = name.toLowerCase();
        if(loadouts.get(kitName) != null){
            HashMap loadoutItems = loadouts.get(kitName);
            Set<ItemStack> keys = loadoutItems.keySet();
            player.getInventory().clear();
            for(ItemStack item : keys) {
                player.getInventory().setItem((Integer)loadoutItems.get(item), item);
            }
        }else{
            player.sendMessage("Loadout doesn't exist.");
        }
    }
}
