package me.extendedhorizons.kitverse.classes.tier1;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Brewer extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Brewer (Player player, Location loc){

        //Location loc = new Location(world,54.500,96,49.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.GOLD_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer's Helmet");
        helmet.setItemMeta(meta4);


        ItemStack chestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.GOLD_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer's Boots");
        boots.setItemMeta(meta2);

        ArmorSet brewerSet = new ArmorSet();
        brewerSet.setHelmet(helmet);
        brewerSet.setChestplate(chestplate);
        brewerSet.setLeggings(leggings);
        brewerSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.WOOD_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Oak Sword");
        held.setItemMeta(heldMeta);

        //ITEMS
        ItemStack brewingStand = new ItemStack(Material.BREWING_STAND_ITEM,1);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.LIGHT_PURPLE + "Brewer");
        this.setTierNumber(1);
        this.setArmor(brewerSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(brewerSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(brewerSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(brewerSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(brewerSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(brewingStand,1);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("brewer",loadoutItems);
    }
}
