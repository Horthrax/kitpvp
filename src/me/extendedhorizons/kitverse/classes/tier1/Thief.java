package me.extendedhorizons.kitverse.classes.tier1;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Thief extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Thief(Player player, Location loc){
        //Location loc = new Location(world,56.500,96,50.500);
        //ARMORSET

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setColor(Color.GRAY);
        meta1.setDisplayName(ChatColor.RED + "Thief's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.RED + "Thief's Boots");
        boots.setItemMeta(meta2);

        ArmorSet thiefSet = new ArmorSet();
        thiefSet.setHelmet(air);
        thiefSet.setChestplate(air);
        thiefSet.setLeggings(leggings);
        thiefSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.WOOD_SWORD);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Wooden Dagger");
        held.setItemMeta(heldMeta);

        //ITEMS
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.RED + "Thief");
        this.setTierNumber(1);
        this.setArmor(thiefSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) 45);
        this.setHeadRotation((byte) 25);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(thiefSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(thiefSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(thiefSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(thiefSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("thief",loadoutItems);
    }
}
