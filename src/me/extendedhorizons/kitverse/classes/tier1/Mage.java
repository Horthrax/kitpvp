package me.extendedhorizons.kitverse.classes.tier1;

import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Mage extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Mage(Player player, Location loc){
        //Location loc = new Location(world,48.500,96,50.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
        LeatherArmorMeta meta4 = (LeatherArmorMeta) helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage's Helmet");
        helmet.setItemMeta(meta4);

        ItemStack chestplate = new ItemStack(Material.GOLD_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage's Leggings");
        meta1.setColor(Color.PURPLE);
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta meta2 = (LeatherArmorMeta) boots.getItemMeta();
        meta2.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage's Boots");
        boots.setItemMeta(meta2);

        ArmorSet mageSet = new ArmorSet();
        mageSet.setHelmet(helmet);
        mageSet.setChestplate(chestplate);
        mageSet.setLeggings(leggings);
        mageSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.STICK);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.addEnchant(Enchantment.DAMAGE_ALL,3,true);
        heldMeta.setDisplayName(ChatColor.WHITE + "Wand");
        held.setItemMeta(heldMeta);

        //ITEMS
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GOLD + "Mage");
        this.setTierNumber(1);
        this.setArmor(mageSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.setYaw((byte) -45);
        this.setHeadRotation((byte) -25);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(mageSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(mageSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(mageSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(mageSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("mage",loadoutItems);
    }
}
