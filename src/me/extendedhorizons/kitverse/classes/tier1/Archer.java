package me.extendedhorizons.kitverse.classes.tier1;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Archer extends Kit {
    private World world = Bukkit.getWorld("world");
    private ItemStack air = new ItemStack(Material.AIR);
    public Archer(Player player, Location loc){
        //Location loc = new Location(world,52.500,96,49.500);
        //ARMORSET
        ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta) chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY + "Archer's Chestplate");
        meta.setColor(Color.GRAY);
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        LeatherArmorMeta meta1 = (LeatherArmorMeta) leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.GRAY + "Archer's Leggings");
        meta1.setColor(Color.GRAY);
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta meta2 = (LeatherArmorMeta) boots.getItemMeta();
        meta2.setColor(Color.SILVER);
        meta2.setDisplayName(ChatColor.GRAY + "Archer's Boots");
        boots.setItemMeta(meta2);

        ArmorSet archerSet = new ArmorSet();
        archerSet.setHelmet(air);
        archerSet.setChestplate(chestplate);
        archerSet.setLeggings(leggings);
        archerSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.BOW);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Cross Bow");
        held.setItemMeta(heldMeta);

        ItemStack arrows = new ItemStack(Material.ARROW,64);
        ItemStack sword = new ItemStack(Material.WOOD_SWORD);
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.GRAY + "Archer");
        this.setTierNumber(1);
        this.setArmor(archerSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(archerSet.getHelmet(),Loadout.headSlot);
        loadoutItems.put(archerSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(archerSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(archerSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(sword,0);
        loadoutItems.put(held,1);
        loadoutItems.put(arrows,8);
        loadoutItems.put(steak,2);
        Loadout.loadouts.put("archer",loadoutItems);
    }
}
