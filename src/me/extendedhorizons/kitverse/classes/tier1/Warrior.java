package me.extendedhorizons.kitverse.classes.tier1;

import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.util.ArmorSet;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Warrior extends Kit {
    private static World world = Bukkit.getWorld("world");
    private static ItemStack air = new ItemStack(Material.AIR);
    public Warrior (Player player, Location loc){
        //Location loc = new Location(world,50.500,96,49.500);
        //ARMORSET
        ItemStack helmet = new ItemStack(Material.CHAINMAIL_HELMET);
        ItemMeta meta4 = helmet.getItemMeta();
        meta4.setDisplayName(ChatColor.DARK_GRAY + "Warrior's Helmet");
        helmet.setItemMeta(meta4);

        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta meta = chestplate.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "Warrior's Chestplate");
        chestplate.setItemMeta(meta);

        ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        ItemMeta meta1 = leggings.getItemMeta();
        meta1.setDisplayName(ChatColor.DARK_GRAY + "Warrior's Leggings");
        leggings.setItemMeta(meta1);

        ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        ItemMeta meta2 = boots.getItemMeta();
        meta2.setDisplayName(ChatColor.DARK_GRAY + "Warrior's Boots");
        boots.setItemMeta(meta2);

        ArmorSet warriorSet = new ArmorSet();
        warriorSet.setHelmet(helmet);
        warriorSet.setChestplate(chestplate);
        warriorSet.setLeggings(leggings);
        warriorSet.setBoots(boots);

        //HELDITEM
        ItemStack held = new ItemStack(Material.WOOD_AXE);
        ItemMeta heldMeta = held.getItemMeta();
        heldMeta.setDisplayName(ChatColor.WHITE + "Axe");
        held.setItemMeta(heldMeta);

        //ITEMS
        ItemStack steak = new ItemStack(Material.COOKED_BEEF,4);

        this.setName(ChatColor.DARK_RED + "Warrior");
        this.setTierNumber(1);
        this.setArmor(warriorSet);
        this.setHeldItem(held);
        this.setWorld(world);
        this.setLocation(loc);
        this.setOwner(player);
        this.setGameProfile(player,this);
        this.loadKit(this,player);
        super.kitList.add(this);

        HashMap<ItemStack,Integer> loadoutItems = new HashMap<>();
        loadoutItems.put(warriorSet.getHelmet(), Loadout.headSlot);
        loadoutItems.put(warriorSet.getChestplate(),Loadout.chestSlot);
        loadoutItems.put(warriorSet.getLeggings(),Loadout.legSlot);
        loadoutItems.put(warriorSet.getBoots(),Loadout.bootSlot);
        loadoutItems.put(held,0);
        loadoutItems.put(steak,1);
        Loadout.loadouts.put("warrior",loadoutItems);
    }
}
