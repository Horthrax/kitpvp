package me.extendedhorizons.kitverse.classes;

import me.extendedhorizons.kitverse.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by horthrax on 7/4/2015.
 */
public class SelectorObject {
    Main main = Main.getInstance();
    Location location;
    World world;
    String kit;

    public SelectorObject(Location location, String kit){
        this.location  = location;
        this.world = location.getWorld();
        this.kit = kit;
        ArmorStand stand = (ArmorStand) this.world.spawnEntity(this.location, EntityType.ARMOR_STAND);
        stand.setVisible(false);
        stand.setBasePlate(false);
        main.getSelectors().addSelector(stand);
        main.getSelectors().addSelectorKit(stand,kit);
    }
}
