package me.extendedhorizons.kitverse.classes;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.blacksmiths.BlacksmithInventory;
import me.extendedhorizons.kitverse.tutorial.Tutorial;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import me.extendedhorizons.kitverse.util.Locations;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.user.User;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

/**
 * Created by horthrax on 8/15/2015.
 */
public class SelectKit implements Listener {
    Main main = Main.getInstance();
    private RankData rankData = new RankData();
    private Locations locations = new Locations();
    Selectors selectors = main.getSelectors();

    @EventHandler
    public void rightClickKit(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof ArmorStand) {
            Player player = event.getPlayer();
            ArmorStand stand = (ArmorStand) event.getRightClicked();
            if (selectors.selectors.contains(stand)) {
                if(selectors.selectorKit.get(stand) != null) {
                    User user = main.getUser(player.getUniqueId());
                    if(!user.getPlayerState().equals(PlayerState.TUTORIAL)) {
                        if (!selectors.selectorKit.get(stand).equalsIgnoreCase("blacksmith") && !selectors.selectorKit.get(stand).equalsIgnoreCase("tutorial")) {
                            String selectorsClassName = selectors.selectorKit.get(stand);
                            Tier tier = Tier.getTierType(selectorsClassName);
                            int tierLevel = Tier.getTierLevel(tier, selectorsClassName);
                            if (user.getTier(tier) >= tierLevel) {
                                player.sendMessage(ChatColor.YELLOW + "You have selected class " + ChatColor.GOLD + selectorsClassName + ChatColor.YELLOW + ".");
                                player.teleport(locations.randomCSpawn());
                                Loadout.applyLoadout(player, selectorsClassName);
                                user.setPlayerState(PlayerState.PVP);
                                user.setSelectedKit(selectorsClassName.toLowerCase());
                                player.setFoodLevel(19);
                            } else {
                                player.sendMessage(ChatColor.RED + "You haven't unlocked " + ChatColor.DARK_RED + "Tier " + ChatColor.DARK_RED + tierLevel + ChatColor.RED + " for this class yet!");
                            }
                        } else if (selectors.selectorKit.get(stand).equalsIgnoreCase("blacksmith")) {
                            new BlacksmithInventory(player);
                        } else if (selectors.selectorKit.get(stand).equalsIgnoreCase("tutorial")) {
                            new Tutorial(player.getUniqueId());
                        }
                    }
                }else{
                    player.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "The class you right clicked was " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "null" + ChatColor.RED.toString() + ChatColor.BOLD + ". Please Contact a staff member IMMEDIATELY!");
                    rankData.sendOnlineStaffMessage(ChatColor.RED.toString() + ChatColor.RED + "A player selected a NULL class. Please notify Extended immediately.");
                }
            }
        }
    }
}
