package me.extendedhorizons.kitverse.economy;

/**
 * Created by horthrax on 12/6/2015.
 */
public enum OperatorType {
    ADD("add"),
    SUBTRACT("subtract"),
    SET("set");

    public String name;

    private OperatorType(String name){
        this.name = name;
    }
    public static String getName(OperatorType type){
        return type.name;
    }
    public static OperatorType fromString(String name){
        for(OperatorType type : OperatorType.values()){
            if(getName(type).equals(name)){
                return type;
            }
        }
        return null;
    }
}
