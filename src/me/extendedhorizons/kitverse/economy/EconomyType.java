package me.extendedhorizons.kitverse.economy;

public enum EconomyType {
    CRYSTALS("crystals"),
    ORBS("orbs"),
    BOTH("both");

    private EconomyType(String name) {
        valueOf(name);
    }
}