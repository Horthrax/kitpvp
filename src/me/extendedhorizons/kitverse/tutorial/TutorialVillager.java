package me.extendedhorizons.kitverse.tutorial;

import me.extendedhorizons.kitverse.util.Locations;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by horthrax in KitVerse on 1/3/2016.
 */
public class TutorialVillager {
    public static HashMap<UUID, Integer> villagerIds = new HashMap<>();
    public static void loadVillager(Player player) {
        WorldServer nmsWorld = ((CraftWorld) player.getWorld()).getHandle();
        EntityVillager villager = new EntityVillager(nmsWorld);
        villager.setLocation(Locations.tutorial.getX(),Locations.tutorial.getY(), Locations.tutorial.getZ(), 0F, 0F);
        Villager villager1 = (Villager) villager.getBukkitEntity();
        villager1.setProfession(Villager.Profession.LIBRARIAN);
        villager.spawnIn(nmsWorld);
        villagerIds.put(player.getUniqueId(),villager.getId());

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        try {
            connection.sendPacket(new PacketPlayOutSpawnEntityLiving(villager));
            connection.sendPacket(new PacketPlayOutEntityHeadRotation(villager, (byte)Locations.tutorial.getYaw()));
            //connection.sendPacket(new PacketPlayOutEntity.PacketPlayOutEntityLook(villager.getBukkitEntity().getEntityId(), (byte)50,  (byte)0, true));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public static void respawn(UUID uuid){
        PlayerConnection connection = ((CraftPlayer) Bukkit.getPlayer(uuid)).getHandle().playerConnection;
        try {
            connection.sendPacket(new PacketPlayOutEntityDestroy(villagerIds.get(uuid)));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        loadVillager(Bukkit.getPlayer(uuid));
    }

}
