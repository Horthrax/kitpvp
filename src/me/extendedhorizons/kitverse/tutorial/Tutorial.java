package me.extendedhorizons.kitverse.tutorial;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by horthrax in KitVerse on 1/3/2016.
 */
public class Tutorial {
    UUID uuid;
    Main main = Main.getInstance();

    String doctorPrefix = ChatColor.DARK_AQUA + "Doc » " + ChatColor.AQUA;

    public Tutorial(UUID uuid){
        this.uuid = uuid;
        playTutorial();
    }

    private void playTutorial(){
        Player player = Bukkit.getPlayer(uuid);
        int sec = 20;
        if(player != null){
            User user = main.getUser(uuid);
            if(!user.getPlayerState().equals(PlayerState.TUTORIAL)) {
                user.setPlayerState(PlayerState.TUTORIAL);

                player.sendMessage(doctorPrefix + "Hmm, maybe if I put this here...");
                player.playSound(player.getLocation(),Sound.ENTITY_EXPERIENCE_ORB_PICKUP,5,2);
                delayedMessage(sec*3, doctorPrefix + "Dagnabbit! *explosion*");
                delayedPlayerSound(sec*3, Sound.ENTITY_GENERIC_EXPLODE);
                delayedMessage(sec*6, doctorPrefix + "Oh hello! How long have you been standing there?");
                delayedPlayerSound(sec*6, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*9, doctorPrefix + "Well never mind that...");
                delayedPlayerSound(sec*9, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*12, doctorPrefix + "People around here call me Doc.");
                delayedPlayerSound(sec*12, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*15, doctorPrefix + "I normally help with the young whippersnappers that come in... ");
                delayedPlayerSound(sec*15, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*18, doctorPrefix + "Anyways, there are 5 classes, Archer, Warrior, Brewer, Mage and Thief.");
                delayedPlayerSound(sec*18, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*21, doctorPrefix + "Each class has 3 tiers.");
                delayedPlayerSound(sec*21, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*24, doctorPrefix + "To use a classes ability, hold shift and right-click your main weapon.");
                delayedPlayerSound(sec*24, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*27, doctorPrefix + "You can get crystals and orbs through killing other players.");
                delayedPlayerSound(sec*27, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*30, doctorPrefix + "After you have some crystals and orbs, you can buy a class upgrades through the Blacksmith.");
                delayedPlayerSound(sec*30, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*33, doctorPrefix + "There are 3 shrines that give you power ups throughout the map.");
                delayedPlayerSound(sec*33, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*36, doctorPrefix + "They regenerate 10 minutes after the collection.");
                delayedPlayerSound(sec*36, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                delayedMessage(sec*39, doctorPrefix + "I think your ready to PvP now! Have fun!");
                delayedPlayerSound(sec*39, Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
                new BukkitRunnable(){
                    @Override
                    public void run(){
                        user.setPlayerState(PlayerState.SPAWN);
                    }
                }.runTaskLater(main,20*36);
            }
        }
    }

    /* TELEPORT ORBS
        public void onUpdate()
    {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        ++this.innerRotation;
        this.dataWatcher.updateObject(8, Integer.valueOf(this.health));
        int i = MathHelper.floor_double(this.posX);
        int j = MathHelper.floor_double(this.posY);
        int k = MathHelper.floor_double(this.posZ);

        if (this.worldObj.provider instanceof WorldProviderEnd && this.worldObj.getBlockState(new BlockPos(i, j, k)).getBlock() != Blocks.fire)
        {
            this.worldObj.setBlockState(new BlockPos(i, j, k), Blocks.fire.getDefaultState());
        }
    }
     */

    private void delayedPlayerSound(int delay, Sound sound){
        Player player = Bukkit.getPlayer(uuid);
        new BukkitRunnable(){
            public void run(){
                if(player != null){
                    player.playSound(player.getLocation(),sound,4,5);
                }
            }
        }.runTaskLater(main,delay);
    }
    private void delayedMessage(int delay, String message){
        Player player = Bukkit.getPlayer(uuid);
        new BukkitRunnable(){
            @Override
            public void run(){
                if(player != null){
                    player.sendMessage(message);
                }
            }
        }.runTaskLater(main,delay);
    }
}
