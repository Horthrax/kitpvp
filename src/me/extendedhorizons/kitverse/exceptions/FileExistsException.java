package me.extendedhorizons.kitverse.exceptions;

/**
 * Created by horthrax in KitVerse on 12/26/2015.
 */
public class FileExistsException extends Exception{
    public FileExistsException(){
        super("File already exists!");
    }
}
