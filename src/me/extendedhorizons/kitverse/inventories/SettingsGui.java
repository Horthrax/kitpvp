package me.extendedhorizons.kitverse.inventories;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.settings.SettingTypes;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.util.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 8/29/2015.
 */
public class SettingsGui implements Listener{
    Main main = Main.getInstance();
    ItemUtil itemUtil = new ItemUtil();

    @EventHandler
    public void open(PlayerInteractEvent event){
        Player player = event.getPlayer();
        Action a = event.getAction();
        if(a == Action.RIGHT_CLICK_BLOCK || a == Action.RIGHT_CLICK_AIR) {
            if (event.getPlayer().getItemInHand().getType().equals(Material.REDSTONE_COMPARATOR)) {
                if(event.getPlayer().getItemInHand().hasItemMeta()){
                    if(event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_GRAY + "›" + ChatColor.RED + "Settings" + ChatColor.DARK_GRAY + "‹")){
                        player.openInventory(settingsInventory(player));
                    }
                }
            }
        }
    }

    public void openToggleGui(Player player ,String togglename){
        User user = main.getUser(player.getUniqueId());
        boolean status = user.getSetting(SettingTypes.getFromString(togglename));
        String statusText;
        if(status){
            statusText = ChatColor.GREEN + " ON ";
        }else{
            statusText = ChatColor.DARK_RED + " OFF ";
        }
        Inventory inventory = Bukkit.createInventory(player, 27, ChatColor.RED + togglename + " Toggle" + statusText + ChatColor.DARK_GRAY.toString() + "»");
        for (int i = 0; i < 27; i++) {
            if (i == 12) {
                inventory.setItem(i, toggleOnItemStack(togglename));
            } else if (i == 14) {
                inventory.setItem(i, toggleOffItemStack(togglename));
            } else {
                inventory.setItem(i, blankSpace());

            }
        }
        player.openInventory(inventory);
    }

    @EventHandler
    public void toggleClickEvent(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            ItemStack item = event.getCurrentItem();
            if (item != null && !item.getType().equals(Material.AIR) && item.hasItemMeta() && !item.getItemMeta().getDisplayName().isEmpty()) {
                if (item.getItemMeta().getDisplayName().contains("Auto Respawn")) {
                    if (item.getType().equals(Material.ANVIL)) {
                        openToggleGui(player, "Auto Respawn");
                    }
                }
                if (item.getItemMeta().getDisplayName().contains("Show KDR")) {
                    if (item.getType().equals(Material.BANNER)) {
                        openToggleGui(player, "Show KDR");
                    }
                }
                if (item.getItemMeta().getDisplayName().contains("Chat Alerts")) {
                    if (item.getType().equals(Material.GOLD_NUGGET)) {
                        openToggleGui(player, "Chat Alerts");
                    }
                }
                if (item.getItemMeta().getLore() != null && !item.getItemMeta().getLore().isEmpty()) {
                    for (String loreString : item.getItemMeta().getLore()) {
                        if (!itemUtil.getToggleName(loreString).isEmpty() && itemUtil.getToggleName(loreString) != null) {
                            String toggleName = itemUtil.getToggleName(loreString);
                            User user = main.getUser(player.getUniqueId());
                            if (user != null) {
                                if (loreString.contains("off")) {
                                    user.setSetting(SettingTypes.getFromString(toggleName), false);
                                } else if (loreString.contains("on")) {
                                    user.setSetting(SettingTypes.getFromString(toggleName), true);
                                }
                                player.closeInventory();
                            }
                        }
                    }
                }
            }
        }
    }
    public ItemStack settingsItemStack(){
        ItemStack settings = new ItemStack(Material.REDSTONE_COMPARATOR);
        ItemMeta meta = settings.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + "Settings" + ChatColor.DARK_GRAY + "‹");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + "Change your settings here!");
        meta.setLore(lore);
        settings.setItemMeta(meta);
        return settings;
    }
    public ItemStack autoRespawnItemSack(Player player){
        User user = main.getUser(player.getUniqueId());
        String displayName = "Auto Respawn";
        ItemStack autoRespawn = new ItemStack(Material.ANVIL);
        ItemMeta meta = autoRespawn.getItemMeta();
        List<String> lore = new ArrayList<>();
        boolean status = user.getSetting(SettingTypes.getFromString(displayName));
        String statusText;
        if(status){
            statusText = ChatColor.GREEN + " ON";
        }else{
            statusText = ChatColor.DARK_RED + " OFF";
        }
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + displayName+ ChatColor.DARK_GRAY + statusText + ChatColor.DARK_GRAY + "‹");
        lore.add(ChatColor.DARK_RED + "Toggle auto respawn.");
        meta.setLore(lore);
        autoRespawn.setItemMeta(meta);
        return autoRespawn;
    }
    public ItemStack kdrItemStack(Player player){
        User user = main.getUser(player.getUniqueId());
        String displayName = "Show KDR";
        ItemStack showKDR = new ItemStack(Material.BANNER, 1, (byte) 1);
        ItemMeta meta = showKDR.getItemMeta();
        List<String> lore = new ArrayList<>();
        boolean status = user.getSetting(SettingTypes.getFromString(displayName));
        String statusText;
        if(status){
            statusText = ChatColor.GREEN + " ON";
        }else{
            statusText = ChatColor.DARK_RED + " OFF";
        }
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + displayName + statusText + ChatColor.DARK_GRAY + "‹");
        lore.add(ChatColor.DARK_RED + "Toggle show kdr.");
        meta.setLore(lore);
        showKDR.setItemMeta(meta);
        return showKDR;
    }
    public ItemStack notificationsItemStack(Player player){
        User user = main.getUser(player.getUniqueId());
        String displayName = "Chat Alerts";
        ItemStack notifications = new ItemStack(Material.GOLD_NUGGET, 1);
        ItemMeta meta = notifications.getItemMeta();
        List<String> lore = new ArrayList<>();
        boolean status = user.getSetting(SettingTypes.getFromString(displayName));
        String statusText;
        if(status){
            statusText = ChatColor.GREEN + " ON";
        }else{
            statusText = ChatColor.DARK_RED + " OFF";
        }
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + displayName + statusText + ChatColor.DARK_GRAY + "‹");
        lore.add(ChatColor.DARK_RED + "Toggle chat alerts.");
        meta.setLore(lore);
        notifications.setItemMeta(meta);
        return notifications;
    }
    public ItemStack toggleOnItemStack(String toggleName){
        ItemStack itemStack = new ItemStack(Material.WOOL, 1, (byte) 5);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + toggleName + ChatColor.DARK_GRAY + "‹");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "Toggle " + toggleName.toLowerCase() + " on.");
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public ItemStack toggleOffItemStack(String toggleName){
        ItemStack itemStack = new ItemStack(Material.WOOL, 1, (byte) 14);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "›" + ChatColor.RED + toggleName + ChatColor.DARK_GRAY + "‹");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_RED + "Toggle " + toggleName.toLowerCase() + " off.");
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }
    public ItemStack blankSpace(){
        ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE,1,(byte) 15);
        ItemMeta meta = blank.getItemMeta();
        meta.setDisplayName(" ");
        blank.setItemMeta(meta);
        return blank;
    }
    public Inventory settingsInventory(Player player){
        Inventory settings = Bukkit.createInventory(player, 27, ChatColor.RED + "Settings" + ChatColor.DARK_GRAY.toString() + "»");
        //0-44
        for(int i = 0; i < 27; i++) {
            if (i == 11) {
                settings.setItem(i, kdrItemStack(player));
            } else if (i==13) {
                settings.setItem(i, autoRespawnItemSack(player));
            } else if (i==15) {
                settings.setItem(i, notificationsItemStack(player));
            }else{
                settings.setItem(i, blankSpace());
            }
        }
        return settings;
    }
}
