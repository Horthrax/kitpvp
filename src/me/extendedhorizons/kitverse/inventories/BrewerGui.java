package me.extendedhorizons.kitverse.inventories;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by horthrax in KitVerse on 12/15/2015.
 */
public class BrewerGui implements Listener{
    Main main = Main.getInstance();
    ItemStack potion1;
    ItemStack potion2;
    ItemStack potion3;
    ItemStack potion4;
    ItemStack potion5;
    ItemStack potion6;

    public void openGui(Player player, int tierNumber){
        potion1 = new ItemStack(Material.POTION);
        PotionMeta meta1 = (PotionMeta) potion1.getItemMeta();
        meta1.setMainEffect(PotionEffectType.HEAL);
        meta1.setDisplayName(ChatColor.RED + "Instant Health");
        potion1.setItemMeta(meta1);

        potion2 = new ItemStack(Material.POTION);
        PotionMeta meta2 = (PotionMeta) potion1.getItemMeta();
        meta2.setMainEffect(PotionEffectType.REGENERATION);
        meta2.setDisplayName(ChatColor.RED + "Regeneration");
        potion2.setItemMeta(meta2);


        potion3 = new ItemStack(Material.POTION);
        PotionMeta meta3 = (PotionMeta) potion1.getItemMeta();
        meta3.setMainEffect(PotionEffectType.FIRE_RESISTANCE);
        meta3.setDisplayName(ChatColor.RED + "Fire Resistance");
        potion3.setItemMeta(meta3);


        potion4 = new ItemStack(Material.POTION);
        PotionMeta meta4 = (PotionMeta) potion1.getItemMeta();
        meta4.setMainEffect(PotionEffectType.SPEED);
        meta4.setDisplayName(ChatColor.RED + "Speed");
        potion4.setItemMeta(meta4);


        potion5 = new ItemStack(Material.POTION);
        PotionMeta meta5 = (PotionMeta) potion1.getItemMeta();
        meta5.setMainEffect(PotionEffectType.DAMAGE_RESISTANCE);
        meta5.setDisplayName(ChatColor.RED + "Resistance");
        potion5.setItemMeta(meta5);


        potion6 = new ItemStack(Material.POTION);
        PotionMeta meta6 = (PotionMeta) potion1.getItemMeta();
        meta6.setMainEffect(PotionEffectType.INCREASE_DAMAGE);
        meta6.setDisplayName(ChatColor.RED + "Strength");
        potion6.setItemMeta(meta6);

        int size = 0;
        switch(tierNumber){
            case(1):
                size = 2;
                break;
            case(2):
                size = 4;
                break;
            case(3):
                size = 6;
                break;
        }

        Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.GOLD + "Potions" + ChatColor.YELLOW + "»");
        switch(size){
            case(2):
                inventory.setItem(0,blankSpace());
                inventory.setItem(1,blankSpace());
                inventory.setItem(2,blankSpace());
                inventory.setItem(3,potion1);
                inventory.setItem(4,blankSpace());
                inventory.setItem(5,potion2);
                inventory.setItem(6,blankSpace());
                inventory.setItem(7,blankSpace());
                inventory.setItem(8,blankSpace());
                break;
            case(4):
                inventory.setItem(0,blankSpace());
                inventory.setItem(1,blankSpace());
                inventory.setItem(2,potion1);
                inventory.setItem(3,potion2);
                inventory.setItem(4,blankSpace());
                inventory.setItem(5,potion3);
                inventory.setItem(6,potion4);
                inventory.setItem(7,blankSpace());
                inventory.setItem(8,blankSpace());
                break;
            case(6):
                inventory.setItem(0,blankSpace());
                inventory.setItem(1,potion1);
                inventory.setItem(2,potion2);
                inventory.setItem(3,potion3);
                inventory.setItem(4,blankSpace());
                inventory.setItem(5,potion4);
                inventory.setItem(6,potion5);
                inventory.setItem(7,potion6);
                inventory.setItem(8,blankSpace());
                break;
        }
        player.openInventory(inventory);
    }

    public ItemStack blankSpace(){
        ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE,1,(byte) 15);
        ItemMeta meta = blank.getItemMeta();
        meta.setDisplayName(" ");
        blank.setItemMeta(meta);
        return blank;
    }

    @EventHandler
    public void brewerGuiClick(InventoryClickEvent event){
        if(event.getInventory().getName().equals(ChatColor.GOLD + "Potions" + ChatColor.YELLOW + "»")){
            Player player = (Player) event.getWhoClicked();
            ItemStack stack = event.getCurrentItem();
            if(stack != null) {
                int cooldown = 0;

                int healPower = 0;
                int regenPower = 0;
                int firePower = 0;
                int speedPower = 0;
                int resistancePower = 0;
                int strengthPower = 0;

                switch (main.getUser(player.getUniqueId()).getTier(Tier.BREWER_TIER)) {
                    case (1):
                        cooldown = 60;
                        healPower = 2;
                        regenPower = 1;
                        break;
                    case (2):
                        cooldown = 50;
                        healPower = 2;
                        regenPower = 2;
                        firePower = 3;
                        speedPower = 2;
                        break;
                    case (3):
                        cooldown = 35;
                        healPower = 3;
                        regenPower = 3;
                        firePower = 4;
                        speedPower = 3;
                        resistancePower = 3;
                        strengthPower = 2;
                        break;
                }
                if (stack.getType().equals(Material.POTION)) {
                    ItemStack potion = event.getCurrentItem();
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Instant Health")) {
                        if (player.getHealth() + healPower > 20) {
                            player.setHealth(20);
                        } else {
                            player.setHealth(player.getHealth() + healPower*2);
                        }
                    }
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Regeneration")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, regenPower*20, regenPower), false);
                    }
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Fire Resistance")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, firePower*20, firePower), false);
                    }
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Speed")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, speedPower*20, speedPower), false);
                    }
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Resistance")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, resistancePower*20, resistancePower), false);
                    }
                    if (potion.getItemMeta().getDisplayName().equals(ChatColor.RED + "Strength")) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, strengthPower*20, strengthPower), false);
                    }
                    main.getUser(player.getUniqueId()).setAbilityCooldown(cooldown * 20);
                    main.getUser(player.getUniqueId()).setPlayerState(PlayerState.ABILITY);
                    player.closeInventory();
                }
            }
        }
    }
}
