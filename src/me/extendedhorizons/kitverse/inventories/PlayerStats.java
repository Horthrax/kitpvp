package me.extendedhorizons.kitverse.inventories;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.economy.EconomyType;
import me.extendedhorizons.kitverse.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 7/1/2015.
 */
public class PlayerStats implements Listener{
    String arrow1 = ">";
    String arrow2 = "<";
    Main main = Main.getInstance();
    @EventHandler
    public void open(PlayerInteractEvent event){
        Player player = event.getPlayer();
        Action a = event.getAction();
        if(a == Action.RIGHT_CLICK_BLOCK || a == Action.RIGHT_CLICK_AIR) {
            if (event.getPlayer().getItemInHand().getType().equals(Material.COMPASS)) {
                if(event.getPlayer().getItemInHand().hasItemMeta()){
                    if(event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.RED.toString() + arrow1 + ChatColor.DARK_RED + "Stats" + ChatColor.RED.toString() + arrow2)){
                        player.openInventory(statsInventory(player));
                    }
                }
            }
        }
    }
    public ItemStack compassStatsItem(){
        ItemStack compass = new ItemStack(Material.COMPASS);
        ItemMeta meta = compass.getItemMeta();
        meta.setDisplayName(ChatColor.RED.toString() + arrow1 + ChatColor.DARK_RED + "Stats" + ChatColor.RED.toString() + arrow2);
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_PURPLE + "PvP Statistics");
        meta.setLore(lore);
        compass.setItemMeta(meta);
        return compass;
    }
    public ItemStack kills(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack kills = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta meta = (SkullMeta) kills.getItemMeta();
        meta.setOwner(player.getName());
        meta.setDisplayName(ChatColor.GRAY.toString() + arrow1 + ChatColor.AQUA.toString() + "Kills" + ChatColor.GRAY.toString() + arrow2);
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + (user.getKills() + ""));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        return kills;
    }
    public ItemStack deaths(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack deaths = new ItemStack(Material.SKULL_ITEM);
        ItemMeta meta = deaths.getItemMeta();
        meta.setDisplayName(ChatColor.GRAY.toString() + arrow1 + ChatColor.DARK_AQUA.toString() + "Deaths" + ChatColor.GRAY.toString() + arrow2);
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + (user.getDeaths() + ""));
        meta.setLore(lore);
        deaths.setItemMeta(meta);
        return deaths;
    }
    public ItemStack kdr(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack kdr = new ItemStack(Material.ANVIL);
        ItemMeta meta = kdr.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "›" + ChatColor.DARK_PURPLE + "KDR" + ChatColor.LIGHT_PURPLE + "‹");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED.toString() + user.getKDR());
        meta.setLore(lore);
        kdr.setItemMeta(meta);
        return kdr;
    }
    public ItemStack rankStack(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack rankStack = new ItemStack(Material.EYE_OF_ENDER);
        ItemMeta meta = rankStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA + "Rank:");
        List<String> lore = new ArrayList<>();
        lore.add(user.getRank().getPrefix());
        meta.setLore(lore);
        rankStack.setItemMeta(meta);
        return rankStack;
    }
    public ItemStack tierStack(Player player, Tier tier){
        User user = main.getUser(player.getUniqueId());
        ItemStack tierStack = new ItemStack(Material.AIR);
        if(tier.equals(Tier.ARCHER_TIER)) {
            tierStack = new ItemStack(Material.BOW);
            ItemMeta meta = tierStack.getItemMeta();
            meta.setDisplayName(ChatColor.GRAY + "Archer Tier:");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.DARK_GRAY.toString() + user.getTier(tier) + "");
            meta.setLore(lore);
            tierStack.setItemMeta(meta);
        }else if(tier.equals(Tier.WARRIOR_TIER)){
            tierStack = new ItemStack(Material.STONE_SWORD);
            ItemMeta meta = tierStack.getItemMeta();
            meta.setDisplayName(ChatColor.DARK_RED + "Warrior Tier:");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.RED.toString() + user.getTier(tier) + "");
            meta.setLore(lore);
            tierStack.setItemMeta(meta);
        }
        else if(tier.equals(Tier.BREWER_TIER)){
            tierStack = new ItemStack(Material.BREWING_STAND_ITEM);
            ItemMeta meta = tierStack.getItemMeta();
            meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer Tier:");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.DARK_PURPLE.toString() + user.getTier(tier) + "");
            meta.setLore(lore);
            tierStack.setItemMeta(meta);
        }
        else if(tier.equals(Tier.THIEF_TIER)){
            tierStack = new ItemStack(Material.BREAD);
            ItemMeta meta = tierStack.getItemMeta();
            meta.setDisplayName(ChatColor.RED + "Thief Tier:");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.DARK_RED.toString() + user.getTier(tier) + "");
            meta.setLore(lore);
            tierStack.setItemMeta(meta);
        }
        else if(tier.equals(Tier.MAGE_TIER)){
            tierStack = new ItemStack(Material.STICK);
            ItemMeta meta = tierStack.getItemMeta();
            meta.setDisplayName(ChatColor.GOLD + "Mage Tier:");
            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.YELLOW.toString() + user.getTier(tier) + "");
            meta.setLore(lore);
            tierStack.setItemMeta(meta);
        }
        return tierStack;
    }
    public ItemStack orbStack(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack experienceStack = new ItemStack(Material.EXP_BOTTLE);
        ItemMeta meta = experienceStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA + "Orbs:");
        List<String> lore = new ArrayList<>();
        lore.add(user.getCurrency(EconomyType.ORBS) + "");
        meta.setLore(lore);
        experienceStack.setItemMeta(meta);
        return experienceStack;
    }
    public ItemStack crystalStack(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack balanceStack = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta meta = balanceStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA + "Crystals:");
        List<String> lore = new ArrayList<>();
        lore.add(user.getCurrency(EconomyType.CRYSTALS) + "");
        meta.setLore(lore);
        balanceStack.setItemMeta(meta);
        return balanceStack;
    }
    public ItemStack placeHolder(){
        ItemStack placeHolder = new ItemStack(Material.SOUL_SAND);
        ItemMeta meta = placeHolder.getItemMeta();
        meta.setDisplayName(ChatColor.WHITE + " ");
        placeHolder.setItemMeta(meta);
        return placeHolder;
    }
    public ItemStack blankSpace(){
        ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE,1,(byte) 15);
        ItemMeta meta = blank.getItemMeta();
        meta.setDisplayName(" ");
        blank.setItemMeta(meta);
        return blank;
    }
    public Inventory statsInventory(Player player){
        Inventory stats = Bukkit.createInventory(player ,45,ChatColor.DARK_RED + "Stats" + ChatColor.RED.toString() + "»");
        //0-44
        for(int i = 0; i < 45; i++){
            if(i == 12){
                stats.setItem(i,kills(player));
            }else
            if(i == 13){
                stats.setItem(i,kdr(player));
            }else
            if(i == 14){
                stats.setItem(i,deaths(player));
            }else
            if(i == 19){
                stats.setItem(i,orbStack(player));
            }else
            if(i == 20){
                stats.setItem(i,tierStack(player, Tier.ARCHER_TIER));
            }else
            if(i == 21){
                stats.setItem(i,tierStack(player, Tier.WARRIOR_TIER));
            }else
            if(i == 22){
                stats.setItem(i,tierStack(player, Tier.BREWER_TIER));
            }else
            if(i == 23){
                stats.setItem(i,tierStack(player, Tier.THIEF_TIER));
            }else
            if(i == 24){
                stats.setItem(i,tierStack(player, Tier.MAGE_TIER));
            }else
            if(i == 25){
                stats.setItem(i,crystalStack(player));
            }else
            if(i == 31){
                stats.setItem(i,rankStack(player));
            }else{
                stats.setItem(i,blankSpace());
            }
        }
        return stats;
    }
}
