package me.extendedhorizons.kitverse.user.settings;

/**
 * Created by horthrax on 12/6/2015.
 */
public enum SettingTypes {
    AUTO_RESPAWN("auto respawn"),
    SHOW_KDR("show kdr"),
    CHAT_ALERTS("chat alerts");

    private String name;
    private SettingTypes(String name){
        this.name = name;
    }

    public static SettingTypes getFromString(String string){
        for(SettingTypes settings : SettingTypes.values()){
            if(settings.name.equalsIgnoreCase(string)){
                return settings;
            }
        }
        return null;
    }
}
