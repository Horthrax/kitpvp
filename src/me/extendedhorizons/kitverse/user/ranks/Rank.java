package me.extendedhorizons.kitverse.user.ranks;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by horthrax on 7/1/2015.
 */

public enum Rank {
    DEFAULT("default", 0, ChatColor.DARK_GRAY + "[" + ChatColor.GRAY.toString() + "Novice✶" + ChatColor.DARK_GRAY + "] ", "default", ChatColor.GRAY, ChatColor.GRAY),
    POLLUX("pollux", 1,ChatColor.GRAY + "[" + ChatColor.GOLD.toString() + "Pollux✷" + ChatColor.GRAY + "] ", "donor", ChatColor.WHITE, ChatColor.WHITE),
    LYNX("lynx", 2,ChatColor.GRAY + "[" + ChatColor.YELLOW.toString() + "Lynx✸" + ChatColor.GRAY + "] ", "donor", ChatColor.WHITE, ChatColor.WHITE),
    CRUX("crux", 3,ChatColor.GRAY + "[" + ChatColor.AQUA.toString() + "Crux"+ ChatColor.BOLD + "✹" + ChatColor.GRAY + "] ", "donor", ChatColor.WHITE, ChatColor.WHITE),
    HELPER("helper", 4, ChatColor.GRAY + "[" + ChatColor.RED.toString() + "Helper" + ChatColor.RED + "✦" + ChatColor.GRAY + "] ", "staff", ChatColor.AQUA, ChatColor.WHITE),
    MOD("mod", 5, ChatColor.GRAY + "[" + ChatColor.DARK_RED.toString() + "Mod" + ChatColor.RED + "✦" + ChatColor.GRAY + "] ", "staff", ChatColor.AQUA, ChatColor.WHITE),
    DEVELOPER("developer", 6, ChatColor.GRAY + "[" + ChatColor.DARK_RED.toString() + "Developer" + ChatColor.RED + "✦" + ChatColor.GRAY + "] ", "staff", ChatColor.AQUA, ChatColor.WHITE),
    OWNER("owner", 6,  ChatColor.GRAY + "[" + ChatColor.DARK_AQUA.toString() + "Owner" + ChatColor.RED + "✦" + ChatColor.GRAY + "] ", "staff", ChatColor.AQUA, ChatColor.WHITE);

    String name, prefix, type;
    ChatColor usernameColor, chatColor;
    int powerLevel;

    Rank(String name, int powerLevel, String prefix, String type, ChatColor usernameColor, ChatColor chatColor){
        this.name = name;
        this.powerLevel = powerLevel;
        this.prefix = prefix;
        this.type = type;
        this.chatColor = chatColor;
        this.usernameColor = usernameColor;
    }


    public String getName(){
        return this.name;
    }

    public static Rank getByName(String name) {
        for(Rank rank : values()) {
            if(rank.getName().equalsIgnoreCase(name)) {
                return rank;
            }
        }
        return Rank.DEFAULT;
    }

    public static boolean isValid(String rank){
        for(Rank r : values()){
            if(r.getName().equalsIgnoreCase(rank)){
                return true;
            }
        }
        return false;
    }
    public String getPrefix(){
        return this.prefix;
    }

    public String getType(){
        return this.type;
    }

    public ChatColor getChatColor(){
        return this.chatColor;
    }

    public ChatColor getUsernameColor(){
        return this.usernameColor;
    }

    public int getPowerLevel(){
        return this.powerLevel;
    }
}
