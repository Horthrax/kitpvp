package me.extendedhorizons.kitverse.user.ranks;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by horthrax on 12/7/2015.
 */
public class RankData {
    public static String staffChatPrefix = ChatColor.AQUA + "Staff" + ChatColor.DARK_AQUA + "» ";
    Main main = Main.getInstance();

    public void sendOnlineStaffMessage(String message){
        if(getOnlineStaff().size() != 0){
            for(Player staff : getOnlineStaff()){
                staff.sendMessage(staffChatPrefix + message);
            }
        }
    }

    public void sendStaffChatMessage(UUID sender, String message){
        if(getOnlineStaff().size() != 0){
            for(Player staff : getOnlineStaff()){
                staff.sendMessage(staffChatPrefix + Bukkit.getPlayer(sender).getName() + ChatColor.LIGHT_PURPLE + " » " + ChatColor.AQUA + message);
            }
        }
    }

    public Collection<? extends Player> getOnlineStaff(){
        Collection<? extends Player> staff = (Collection<? extends Player>) main.getUsers().stream().filter(s -> s.isStaff()).map(s -> s.isStaff());
        return staff;
    }

    public int getPowerLevel(CommandSender sender){
        if(sender instanceof Player){
            Player player = (Player) sender;
            return main.getUser(player.getUniqueId()).getRank().getPowerLevel();
        }else if(sender.isOp()){
            return 6;
        }
        return 0;
    }

    public int getPowerLevel(UUID uuid){
        return main.getUser(uuid).getRank().getPowerLevel();
    }

    public int getPowerLevel(Player player){
        return main.getUser(player.getUniqueId()).getRank().getPowerLevel();
    }

    public boolean isStaff(CommandSender sender){
        if(sender instanceof Player){
            Player player = (Player) sender;
            return main.getUser(player.getUniqueId()).getRank().getType().equalsIgnoreCase("staff") || sender.isOp();
        }
        return true;
    }

    public boolean isStaff(UUID uuid){
        if(main.getUser(uuid).getRank().getType().equalsIgnoreCase("staff")){
            return true;
        }
        return false;
    }

    public boolean isStaff(Player player){
        return isStaff(player);
    }

}
