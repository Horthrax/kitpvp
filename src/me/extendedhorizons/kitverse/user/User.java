package me.extendedhorizons.kitverse.user;

import com.mojang.authlib.GameProfile;
import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.cosmetics.chattab.Tab;
import me.extendedhorizons.kitverse.economy.EconomyType;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import me.extendedhorizons.kitverse.user.settings.SettingTypes;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by horthrax on 6/27/2015.
 */

@SuppressWarnings("unused")

public class User {
    Main main = Main.getInstance();
    public UUID uuid;
    public String username;
    public Rank rank = null;
    public String selectedKit = null;
    public int kills = -1;
    public int deaths = -1;
    public int highestKS = -1;
    public int crystals = -1;
    public int orbs = -1;
    public HashMap<Tier, Integer> tierData = new HashMap<>();
    public HashMap<SettingTypes, Boolean> settingData = new HashMap<>();
    public HashMap<UUID, Integer> abilityCooldown = new HashMap<>();
    public PlayerState playerState;
    public RankData rankData;

    public User(UUID uuid, String username) {
        this.uuid = uuid;
        this.username = username;
        this.settingData.put(SettingTypes.AUTO_RESPAWN, false);
        this.settingData.put(SettingTypes.SHOW_KDR, true);
        this.settingData.put(SettingTypes.CHAT_ALERTS, true);
        getTier(Tier.ARCHER_TIER);
        getTier(Tier.WARRIOR_TIER);
        getTier(Tier.BREWER_TIER);
        getTier(Tier.MAGE_TIER);
        getTier(Tier.THIEF_TIER);
        getRank();
        getCurrency(EconomyType.ORBS);
        getCurrency(EconomyType.CRYSTALS);
        getDeaths();
        getKills();
        getHighestKS();
        setPlayerState(PlayerState.SPAWN);
        main.users.add(this);
        main.getDB().updateUserName(this.uuid, this.username);
    }
    public void resetPlayerName(Player player) {
        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer nmsWorld = ((CraftWorld) player.getWorld()).getHandle();

        PlayerInteractManager pim = new PlayerInteractManager(nmsWorld);
        EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, new GameProfile(player.getUniqueId(), player.getName()), pim);

        npc.setLocation(0, 0, 0, 0F, 0F);
        npc.setInvisible(true);
        npc.spawnIn(nmsWorld);

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        try {
            connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, npc));
            connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                connection.sendPacket(new PacketPlayOutEntityDestroy(npc.getBukkitEntity().getEntityId()));
                Tab tab = new Tab();
                tab.initTab(player);
            }
        }.runTaskLater(main, 2);
    }
    public User getUser(){
        return this;
    }
    public String getUsername(){
        return this.username;
    }
    public Boolean isStaff(){
        return rankData.isStaff(this.getPlayer());
    }
    //Online Setters
    public void setSetting(SettingTypes setting, boolean status){
        this.settingData.put(setting,status);
    }
    public void setRank(Rank rank) {
        this.rank = rank;
    }
    public void setTier(Tier tier, int tierNumber){
        if(tierNumber <= 3) {
            this.tierData.put(tier, tierNumber);
        }else if(tierNumber > 3){
            this.tierData.put(tier, 3);
        }
    }
    public void setPlayerState(PlayerState playerState){
        this.playerState = playerState;
    }
    public void setSelectedKit(String selectedKit){
        this.selectedKit = selectedKit;
    }
    public void setDeaths(int amount){
        this.deaths = amount;
    }
    public void setKills(int amount){
        this.kills = amount;
    }
    public void setCurrency(EconomyType type, int amount){
        switch(type){
            case ORBS:
                this.orbs = amount;
                break;
            case CRYSTALS:
                this.orbs = amount;
                break;
        }
    }
    public void updateDeaths(int amount){
        this.deaths += amount;
    }
    public void updateKills(int amount){
        this.kills += amount;
    }
    public void updateCurrency(EconomyType type, int amount){
        switch(type.name()){
            case "ORBS":
                this.orbs += amount;
                break;
            case "CRYSTALS":
                this.crystals += amount;
                break;
        }
    }
    public void setAbilityCooldown(int ticks) {
        this.abilityCooldown.put(this.uuid, ticks);
        int cooldownBar = ticks / 7;
        getPlayer().setExp(1);
        new BukkitRunnable() {
            int i = 128;
            @Override
            public void run() {
                if (Bukkit.getPlayer(getUUID()) != null) {
                    if (getPlayerState().equals(PlayerState.PVP) || getPlayerState().equals(PlayerState.ABILITY)) {
                        if (i > 0 && getPlayer().getExp() >= 0) {
                            i--;
                            Float f = 1F / 112F;
                            getPlayer().setExp(getPlayer().getExp() - f); //0.142 with 8
                            //getPlayer().setExp(0.0F);
                        } else {
                            abilityCooldown.remove(uuid);
                            this.cancel();
                        }
                    }else{
                        abilityCooldown.remove(uuid);
                        this.cancel();
                    }
                } else {
                    abilityCooldown.remove(uuid);
                    this.cancel();
                }
            }
        }.runTaskTimer(main, 0, cooldownBar / 16);
    }
    //Online Getters
    public boolean getSetting( SettingTypes setting){
        return this.settingData.get(setting);
    }
    public int getKills(){
        if(this.kills == -1) {
            ResultSet rs = main.getDB().query("SELECT kills FROM kit_verse WHERE uuid = ?", getUUID().toString());
            try {
                if (rs.next()) {
                    int kills = rs.getInt("kills");
                    this.kills = kills;
                    return kills;
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return this.kills;
        }
    }
    public int getDeaths(){
        if(this.deaths == -1) {
            ResultSet rs = main.getDB().query("SELECT deaths FROM kit_verse WHERE uuid = ?", getUUID().toString());
            try {
                if (rs.next()) {
                    int deaths = rs.getInt("deaths");
                    this.deaths = deaths;
                    return deaths;
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return this.deaths;
        }
    }
    public int getTier(Tier tier){
        if(this.tierData.get(tier) == null) {
            ResultSet rs = main.getDB().query("SELECT " + tier.getName().toLowerCase() + " FROM kit_verse WHERE uuid = ?", getUUID().toString());
            try {
                if (rs.next()) {
                    int tierNumber = rs.getInt(tier.getName().toLowerCase());
                    this.tierData.put(tier,tierNumber);
                    return tierNumber;
                } else {
                    return 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 1;
            }
        }else {
            return this.tierData.get(tier);
        }
    }
    public int getCurrency(EconomyType type){
        int amount = 0;
        switch(type.name()){
            case "ORBS":
                amount = orbs;
                break;
            case "CRYSTALS":
                amount = crystals;
                break;
        }
        if(amount == -1) {
            int currency = main.getDB().getCurrency(type,uuid);
            setCurrency(type, currency);
            return currency;
        }else{
            return amount;
        }
    }
    public int getHighestKS(){
        if(this.highestKS == -1){
            ResultSet rs = main.getDB().query("SELECT best_killstreak FROM kit_verse WHERE uuid = ?", getUUID().toString());
            try {
                if (rs.next()) {
                    int highestKS = rs.getInt("best_killstreak");
                    this.highestKS = highestKS;
                    return highestKS;
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return this.highestKS;
        }
    }
    public PlayerState getPlayerState(){
        return this.playerState;
    }
    public double getKDR() {
        if(getKills() > 0) {
            if(getDeaths() == 0){
                return getKills();
            }else{
                return (Math.round(((double) getKills() / (double) getDeaths()) * 100) / (double) 100);
            }
        }else{
            return 0;
        }
    }
    public void resetTiers(){
        setTier(Tier.ARCHER_TIER,1);
        setTier(Tier.WARRIOR_TIER,1);
        setTier(Tier.BREWER_TIER,1);
        setTier(Tier.THIEF_TIER,1);
        setTier(Tier.MAGE_TIER,1);
    }
    public Player getOnlinePlayer(String string){
        for(Player player : Bukkit.getOnlinePlayers()){
            if(player.getName().equals(string)){
                return player;
            }
        }
        return null;
    }
    public Player getOnlinePlayer(UUID uuid){
        for(Player player : Bukkit.getOnlinePlayers()){
            if(player.getUniqueId().equals(uuid)) {
                return player;
            }
        }
        return null;
    }
    public Player getPlayer(){
        return Bukkit.getPlayer(getUUID());
    }
    public UUID getUUID(){
        return this.uuid;
    }
    public String getSelectedKit(){
        return this.selectedKit;
    }
    public Rank getRank(){
        if(this.rank == null) {
            ResultSet rs = main.getDB().query("SELECT rank FROM kit_verse WHERE uuid = ?", getUUID().toString());
            try {
                if (rs.next()) {
                    Rank rank = Rank.getByName(rs.getString("rank"));
                    this.rank = rank;
                    return rank;
                } else {
                   return Rank.DEFAULT;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return Rank.DEFAULT;
            }
        }else{
            return this.rank;
        }
    }
    public boolean hasAbilityCooldown(){
        return this.abilityCooldown.containsKey(this.uuid);
    }

    public void updateDatabase(){
        main.getDB().executeUpdate("UPDATE kit_verse SET rank = ? WHERE uuid = ?", getRank().getName().toLowerCase(), getUUID().toString());
        main.getDB().executeUpdate("UPDATE kit_verse SET orbs = ? WHERE uuid = ?", getCurrency(EconomyType.ORBS), getUUID().toString());
        main.getDB().executeUpdate("UPDATE kit_verse SET crystals = ? WHERE uuid = ?", getCurrency(EconomyType.CRYSTALS), getUUID().toString());
        main.getDB().executeUpdate("UPDATE kit_verse SET deaths = ? WHERE uuid = ?", getDeaths(), getUUID().toString());
        main.getDB().executeUpdate("UPDATE kit_verse SET kills = ? WHERE uuid = ?", getKills(), getUUID().toString());
        for(Tier tier : Tier.values()) {
            main.getDB().executeUpdate("UPDATE kit_verse SET " + tier + " = ? WHERE uuid = ?", getTier(tier), getUUID().toString());
        }
    }
}