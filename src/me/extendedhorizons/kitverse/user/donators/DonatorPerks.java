package me.extendedhorizons.kitverse.user.donators;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 7/2/2015.
 */
public class DonatorPerks {
    public ItemStack donationStationStack(){
        ItemStack donationStation = new ItemStack(Material.NOTE_BLOCK);
        ItemMeta meta = donationStation.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_PURPLE + "›" + ChatColor.LIGHT_PURPLE + "Donator Perks" + ChatColor.DARK_PURPLE + "‹");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + "This is where you can change particle effects" + "and much much more!");
        meta.setLore(lore);
        donationStation.setItemMeta(meta);
        return donationStation;
    }
}
