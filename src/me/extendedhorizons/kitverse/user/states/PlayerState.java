package me.extendedhorizons.kitverse.user.states;

/**
 * Created by horthrax on 12/8/2015.
 */
public enum PlayerState {
    OFFLINE,SPAWN,PVP,ABILITY,RESPAWN,TUTORIAL
}
