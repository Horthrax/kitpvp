package me.extendedhorizons.kitverse.orbs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

import java.util.Random;

/**
 * Created by horthrax in KitVerse on 1/3/2016.
 */

public class Orb {
    int innerRotation;
    double yaw;
    double pitch;
    Random rand = new Random();
    Location location;
    ArmorStand core;
    EulerAngle prevAngle;
    EulerAngle nextAngle;

    //93.5

    public Orb(Location location){
        this.innerRotation = this.rand.nextInt(90);
        this.core = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        core.setHelmet(new ItemStack(Material.STAINED_GLASS));
        core.setVisible(false);
        core.setGravity(false);
        core.setBasePlate(false);
        core.setSmall(true);
        Bukkit.broadcastMessage(location + "");
        this.location = location;
        this.yaw = 0;
        this.pitch = 0;
    }

    public void onUpdate(){
        pitch += 1;
        yaw += 1;

        prevAngle = core.getHeadPose();

        nextAngle = new EulerAngle(pitch/10,yaw/30,0);
        core.setHeadPose(nextAngle);


        //Bukkit.broadcastMessage("Previous angle: " + prevAngle.getX() + " " + prevAngle.getY() + " " + prevAngle.getZ() + " ; " + "Current angle: "  + core.getHeadPose().getX() + " " + core.getHeadPose().getY() + " " + core.getHeadPose().getZ() + " ; ");
        //Bukkit.broadcastMessage(core.getHeadPose().get);
        //Bukkit.broadcastMessage(pitch%5 + "");

        if(pitch % (180/3) == 0) { //9 rotations || I have no fucking clue why this works... like my god. pitch % ((2810/9)/5) == 0
            //Bukkit.broadcastMessage("Current pitch: " + pitch);
            core.setHelmet(new ItemStack(Material.STAINED_GLASS,1,(byte)rand.nextInt(15)));
        }
    }

    public static int floor_float(float value) {
        int i = (int)value;
        return value < (float)i ? i - 1 : i;
    }

    public static int floor_double(double value) {
        int i = (int)value;
        return value <= (double)i ? i - 1 : i;
    }

}
