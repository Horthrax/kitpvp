package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by horthrax on 1/1/2016.
 */
public class GamemodeExecutor implements CommandExecutor {

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().toLowerCase().matches("creative|survival|spectator|adventure")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (rankData.getPowerLevel(sender) >= 5) {
                    GameMode mode = GameMode.valueOf(cmd.getName().toUpperCase());
                    if (player.getGameMode() == mode) {
                        player.sendMessage(ChatColor.YELLOW + "You're already in " + ChatColor.GOLD + mode.toString().toUpperCase() + ChatColor.YELLOW + "!");
                    } else {
                        player.sendMessage(ChatColor.YELLOW + "Successfully changed your gamemode to: " + ChatColor.GOLD + mode.toString().toUpperCase() + ChatColor.YELLOW + ".");
                        player.setGameMode(mode);
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Sorry, you don't have permission to run this command.");
                }
            }else if (args.length == 1) {
                Player targetPlayer = Bukkit.getPlayer(args[0]);
                if (targetPlayer != null) {
                    User targetUser = Main.getInstance().getUser(targetPlayer.getUniqueId());
                    sender.sendMessage(ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " has " + ChatColor.GOLD + targetUser.getBalance() + ChatColor.AQUA + " crystals and " + ChatColor.DARK_AQUA + targetUser.getExperience() + ChatColor.AQUA + " orbs.");
                } else {
                    int crystals = Main.getInstance().getDB().getBalanceExact(Main.getInstance().getDB().getUUID(args[0]));
                    int orbs = Main.getInstance().getDB().getExperienceExact(Main.getInstance().getDB().getUUID(args[0]));
                    sender.sendMessage(ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " has " + ChatColor.GOLD + crystals + ChatColor.AQUA + " crystals and " + ChatColor.DARK_AQUA + orbs + ChatColor.AQUA + " orbs");
                    return true;
                }
            }
        }
        return false;
    }
}
