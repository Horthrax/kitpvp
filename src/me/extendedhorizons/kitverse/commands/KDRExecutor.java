package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by horthrax on 1/1/2016.
 */
public class KDRExecutor implements CommandExecutor {

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("setkills")) {
            if (rankData.isStaff(sender) && rankData.getPowerLevel(sender) == 6) {
                if (args.length == 2) {
                    if (Main.getInstance().getDB().checkExists(Main.getInstance().getDB().getUUID(args[0]))) {
                        Player targetPlayer = Bukkit.getPlayer(args[0]);
                        String userName = args[0];
                        String amount = args[1];
                        if (targetPlayer != null) {
                            User targetUser = Main.getInstance().getUser(targetPlayer.getUniqueId());
                            if (sender instanceof Player) {
                                Player senderPlayer = (Player) sender;
                                User senderUser = Main.getInstance().getUser(senderPlayer.getUniqueId());
                                Rank rank = senderUser.getRank();

                                Bukkit.broadcastMessage(Integer.parseInt(amount) + "");
                                senderPlayer.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + args[1] + ChatColor.AQUA + " kills.");
                                targetPlayer.sendMessage(ChatColor.DARK_AQUA + rank.getPrefix() + rank.getUsernameColor() + senderPlayer.getName() + ChatColor.AQUA + " has set your kills to " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + ".");
                                targetUser.setKills(Integer.parseInt(amount));
                                return true;
                            }
                            Bukkit.broadcastMessage(Integer.parseInt(amount) + "");
                            targetUser.setKills(Integer.parseInt(amount));
                            targetPlayer.sendMessage(ChatColor.DARK_AQUA + "CONSOLE" + ChatColor.AQUA + " has changed your kills to " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + ".");
                            sender.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + " kills.");
                            return true;
                        } else {
                            Bukkit.broadcastMessage(Integer.parseInt(amount) + "");
                            sender.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + " kills.");
                            Main.getInstance().getDB().setKillsExact(Main.getInstance().getDB().getUUID(userName), Integer.parseInt(amount));
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "That player hasn't joined this server before.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Incorrect arguments, usage: " + ChatColor.DARK_RED + "/setkills <player> <amount>");
                    return true;
                }
            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        if (cmd.getName().equalsIgnoreCase("setdeaths")) {
            if (rankData.isStaff(sender) && rankData.getPowerLevel(sender) == 6) {
                if (args.length == 2) {
                    if (Main.getInstance().getDB().checkExists(Main.getInstance().getDB().getUUID(args[0]))) {
                        Player targetPlayer = Bukkit.getPlayer(args[0]);
                        String userName = args[0];
                        String amount = args[1];
                        if (targetPlayer != null) {
                            User targetUser = Main.getInstance().getUser(targetPlayer.getUniqueId());
                            if (sender instanceof Player) {
                                Player senderPlayer = (Player) sender;
                                User senderUser = Main.getInstance().getUser(senderPlayer.getUniqueId());
                                Rank rank = senderUser.getRank();
                                targetPlayer.sendMessage(ChatColor.DARK_AQUA + rank.getPrefix() + rank.getUsernameColor() + senderPlayer.getName() + ChatColor.AQUA + " has set your deaths to " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + ".");
                                senderPlayer.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + " deaths.");
                                targetUser.setDeaths(Integer.parseInt(amount));
                                return true;
                            }
                            targetUser.setDeaths(Integer.parseInt(amount));
                            targetPlayer.sendMessage(ChatColor.DARK_AQUA + "CONSOLE" + ChatColor.AQUA + " has changed your deaths to " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + ".");
                            sender.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + " deaths.");
                            return true;
                        } else {
                            sender.sendMessage(ChatColor.DARK_AQUA + userName + ChatColor.AQUA + " now has " + ChatColor.DARK_AQUA + amount + ChatColor.AQUA + " deaths.");
                            Main.getInstance().getDB().setDeathsExact(Main.getInstance().getDB().getUUID(userName), Integer.parseInt(amount));
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "That player hasn't joined this server before.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Incorrect arguments, usage: " + ChatColor.DARK_RED + "/setdeaths <player> <amount>");
                    return true;
                }
            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        return false;
    }

    public void sendNoPerms(CommandSender player) {
        player.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to run this command.");
    }
}
