package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.economy.EconomyType;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by horthrax in KitVerse on 1/6/2016.
 */
public class BalanceExecutor implements CommandExecutor{

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("balance")) {
            if (args.length == 1 && rankData.getPowerLevel(sender) >= 2) { //Lynx is power level 2 (2nd donor rankData)
                Player targetPlayer = Bukkit.getPlayer(args[0]);
                if (targetPlayer != null) {
                    User targetUser = Main.getInstance().getUser(targetPlayer.getUniqueId());
                    sender.sendMessage(ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " has " + ChatColor.GOLD + targetUser.getCurrency(EconomyType.CRYSTALS) + ChatColor.AQUA + " crystals and " + ChatColor.DARK_AQUA + targetUser.getExperience() + ChatColor.AQUA + " orbs.");
                } else {
                    int crystals = Main.getInstance().getDB().getOrbs(Main.getInstance().getDB().getUUID(args[0]));
                    int orbs = Main.getInstance().getDB().getExperienceExact(Main.getInstance().getDB().getUUID(args[0]));
                    sender.sendMessage(ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " has " + ChatColor.GOLD + crystals + ChatColor.AQUA + " crystals and " + ChatColor.DARK_AQUA + orbs + ChatColor.AQUA + " orbs");
                    return true;
                }
            } else if (args.length == 0 && sender instanceof Player) {
                Player player = (Player) sender;
                User user = Main.getInstance().getUser(player.getUniqueId());
                sender.sendMessage(ChatColor.AQUA + "You have " + ChatColor.GOLD + user.getBalance()
                        + ChatColor.AQUA + " crystals and " + ChatColor.DARK_AQUA + user.getExperience() + ChatColor.AQUA + " ❍.");
                return true;
            } else if (args.length == 1) {
                sender.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you have to have rank " + ChatColor.DARK_RED + ChatColor.BOLD + "Lynx" + ChatColor.RED + " to have access to other players amount of crystals.");
                return true;
            } else {
                return true;
            }
        }
        return false;
    }

    public void sendNoPerms(CommandSender player) {
        player.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to run this command.");
    }
}