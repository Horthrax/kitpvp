package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by horthrax on 1/1/2016.
 */
public class RanksExecutor implements CommandExecutor {

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("setrank")) {
            if (rankData.isStaff(sender) && rankData.getPowerLevel(sender) == 6) {
                if (args.length == 2) {
                    if (Rank.isValid(args[1])) {
                        UUID uuid = Main.getInstance().getDB().getUUID(args[0]);
                        if (uuid != null) {
                            if (Main.getInstance().getDB().checkExists(Main.getInstance().getDB().getUUID(args[0]))) {
                                UUID targetUUID = Main.getInstance().getDB().getUUID(args[0]);
                                if (Rank.isValid(args[1])) {
                                    Rank theirRank = Rank.getByName(args[1]);
                                    try {
                                        Player onlineTarget = Bukkit.getPlayer(args[0]);
                                        if (onlineTarget != null) {
                                            onlineTarget.sendMessage(ChatColor.AQUA + "Your rank was changed to " + ChatColor.DARK_AQUA + args[1] + ChatColor.AQUA + " by " + ChatColor.DARK_AQUA + sender.getName());
                                            Main.getInstance().getUser(onlineTarget.getUniqueId()).setRank(theirRank);
                                            return true;
                                        } else {
                                            Main.getInstance().getDB().setRankExact(targetUUID, theirRank);
                                            sender.sendMessage(ChatColor.AQUA + "You successfully changed " + ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " rank to " + ChatColor.DARK_AQUA + args[1]);
                                            return true;
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    sender.sendMessage(ChatColor.RED + "Error, That rank is invalid.");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED + "That player hasn't joined this server before.");
                                return true;
                            }
                        } else {
                            sender.sendMessage(ChatColor.RED + "That player hasn't joined this server before.");
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "That rank does not exist.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Incorrect arguments, usage: " + ChatColor.DARK_RED + "/setrank <player> <rank>");
                    return true;
                }
            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        return false;
    }

    public void sendNoPerms(CommandSender player) {
        player.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to run this command.");
    }
}
