package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by horthrax on 1/1/2016.
 */
public class TierExecutor implements CommandExecutor {

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("settier")) {
            String tierIncorrectArgs = ChatColor.RED + "Incorrect arguments, usage: " + ChatColor.DARK_RED + "/settier <player> <tier> <1-3>";
            if (rankData.isStaff(sender) && rankData.getPowerLevel(sender) == 6) {
                if (args.length == 3) {
                    int tierNumber;
                    UUID uuid = Main.getInstance().getDB().getUUID(args[0]);
                    if (uuid != null) {
                        if (Main.getInstance().getDB().checkExists(uuid)) {
                            Tier tier;
                            if (args[2].equals("1") || args[2].equals("2") || args[2].equals("3")) {
                                tierNumber = Integer.parseInt(args[2]);
                                tier = Tier.getTierFromString(args[1]);
                                if (tier != null) {
                                    Player targetPlayer = Bukkit.getPlayer(args[0]);
                                    if (targetPlayer != null) {
                                        if (sender instanceof Player) {
                                            Player player = (Player) sender;
                                            User user = Main.getInstance().getUser(player.getUniqueId());
                                            targetPlayer.sendMessage(ChatColor.AQUA + "You're " + ChatColor.DARK_AQUA + tier.getName() + ChatColor.AQUA + " was set to " + ChatColor.DARK_AQUA + tierNumber + ChatColor.AQUA + " by " + ChatColor.DARK_AQUA + user.getRank().getPrefix() + user.getRank().getUsernameColor() + player.getName() + ChatColor.AQUA + ".");
                                        }
                                        targetPlayer.sendMessage(ChatColor.AQUA + "You're " + ChatColor.DARK_AQUA + tier.getName() + ChatColor.AQUA + " was set to " + ChatColor.DARK_AQUA + tierNumber + ChatColor.AQUA + " by " + ChatColor.DARK_AQUA + sender.getName() + ChatColor.AQUA + ".");
                                        Main.getInstance().getUser(targetPlayer.getUniqueId()).setTier(tier, tierNumber);
                                        return true;
                                    } else {
                                        Main.getInstance().getDB().setTierExact(Main.getInstance().getDB().getUUID(args[0]), tier, tierNumber);
                                        sender.sendMessage(ChatColor.AQUA + "You successfully changed " + ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + "'s " + ChatColor.DARK_AQUA + tier.name().toLowerCase() + ChatColor.AQUA + " to " + ChatColor.DARK_AQUA + tierNumber + ChatColor.AQUA + ".");
                                        return true;
                                    }
                                }
                                sender.sendMessage(ChatColor.RED + "Invalid Tier.");
                                return true;
                            }
                            sender.sendMessage(tierIncorrectArgs);
                            return true;
                        } else {
                            sender.sendMessage(ChatColor.RED + "The specified player hasn't joined this server before.");
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "The specified player hasn't joined this server before.");
                        return true;
                    }
                } else if (args.length == 2) {
                    sender.sendMessage(tierIncorrectArgs);
                    return true;
                }
            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        if (cmd.getName().equalsIgnoreCase("resettiers")) {
            if (rankData.isStaff(sender) && rankData.getPowerLevel(sender) == 6) {
                if (args.length == 1) {
                    if (Main.getInstance().getDB().checkExists(Main.getInstance().getDB().getUUID(args[0]))) {
                        Player targetPlayer = Bukkit.getPlayer(args[0]);
                        if (targetPlayer != null) {
                            Main.getInstance().getUser(targetPlayer.getUniqueId()).resetTiers();
                            if (sender instanceof Player) {
                                Player player = (Player) sender;
                                User user = Main.getInstance().getUser(player.getUniqueId());
                                targetPlayer.sendMessage(ChatColor.AQUA + "Your tiers were reset by " + user.getRank().getPrefix() + user.getRank().getUsernameColor() + player.getName() + ChatColor.AQUA + ".");
                                sender.sendMessage(ChatColor.AQUA + "You successfully reset " + ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " tier's");
                                return true;
                            }
                        } else {
                            Main.getInstance().getDB().resetTiersExact(Main.getInstance().getDB().getUUID(args[0]));
                            sender.sendMessage(ChatColor.AQUA + "You successfully reset " + ChatColor.DARK_AQUA + args[0] + ChatColor.AQUA + " tier's");
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "That player hasn't joined this server before.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Incorrect arguments, usage: " + ChatColor.DARK_RED + "/resettiers <player>");
                    return true;
                }
            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        return false;
    }

    public void sendNoPerms(CommandSender player) {
        player.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to run this command.");
    }
}
