package me.extendedhorizons.kitverse.commands;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by horthrax on 1/1/2016.
 */
public class ClearChatExecutor implements CommandExecutor {

    RankData rankData = new RankData();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("clearchat")) {
            if (rankData.isStaff(sender)) {
                for (Player players : Bukkit.getOnlinePlayers()) {
                    for (int i = 0; i < 250; i++) {
                        players.sendMessage("");
                    }
                }
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    User user = Main.getInstance().getUser(player.getUniqueId());
                    Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "Chat was cleared by " + user.getRank().getPrefix() + ChatColor.AQUA.toString() + player.getName() + ChatColor.LIGHT_PURPLE + ".");
                    return true;
                } else {
                    Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + "Chat was cleared by " + ChatColor.AQUA + "CONSOLE" + ChatColor.LIGHT_PURPLE + ".");
                    return true;
                }

            } else {
                sendNoPerms(sender);
                return true;
            }
        }
        return false;
    }


    public void sendNoPerms(CommandSender player) {
        player.sendMessage(ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to run this command.");
    }
}
