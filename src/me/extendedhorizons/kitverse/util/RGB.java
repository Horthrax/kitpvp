package me.extendedhorizons.kitverse.util;

/**
 * Created by horthrax on 12/10/2015.
 */
public class RGB {
    int red;
    int green;
    int blue;
    float redFloat;
    float greenFloat;
    float blueFloat;

    public RGB(int red, int green, int blue){
        this.red = red;
        this.green = green;
        this.blue = blue;
        correctFloats();
    }

    public void correctFloats(){
        final double correctRGBRatio = 3.92156862745;
        this.redFloat = (float) ((correctRGBRatio * red) / 1000);
        this.greenFloat  = (float) ((correctRGBRatio * green) / 1000);
        this.blueFloat  = (float) ((correctRGBRatio * blue) / 1000);
    }

    public int getRed(){
        return red;
    }

    public int getGreen(){
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public float getRedFloat(){
        return redFloat;
    }

    public float getGreenFloat(){
        return greenFloat;
    }

    public float getBlueFloat(){
        return blueFloat;
    }
}
