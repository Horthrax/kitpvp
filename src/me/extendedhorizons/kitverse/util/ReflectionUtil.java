package me.extendedhorizons.kitverse.util;

import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * Created by horthrax on 12/10/2015.
 */
public class ReflectionUtil {
    public Constructor<?> getConstructor(Class clazz, Class... params) {
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor c : constructors) {
            Class[] cParams = c.getParameterTypes();
            if (c.getParameterCount() == params.length && Arrays.equals(cParams, params)) {
                return c;
            }
        }
        return null;
    }
}