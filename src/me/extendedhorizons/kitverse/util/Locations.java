package me.extendedhorizons.kitverse.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by horthrax on 6/27/2015.
 */
public class Locations {
    public static ArrayList<Location> cSpawnL;
    static World world = Bukkit.getWorld("world");
    public static Location spawn = new Location(world, 52.500, 96, 57.500, -180, 0);
    public static Location tutorial = new Location(world, 54.1,95,62, 110, 0);
    public static Location orb = new Location(world, 49.5, 96, 54.5, 0, 0);

    public Locations(){
        cSpawnL = new ArrayList<>();

        Location cspawn1 = new Location(world, -9.5, 35, 115.5);
        Location cspawn2 = new Location(world, -39, 33, 91);
        Location cspawn3 = new Location(world, -2.5, 35, 78);
        Location cspawn4 = new Location(world, -3, 32, 12);
        Location cspawn5 = new Location(world, 10.0, 36, 155);
        Location cspawn6 = new Location(world, 44, 34, 12);
        Location cspawn7 = new Location(world, 84.5, 36, 31.5);
        Location cspawn8 = new Location(world, 109, 40, 51.5);
        Location cspawn9 = new Location(world, 131.5, 39, 31.5);
        Location cspawn10 = new Location(world, 142.5, 39, 54);
        Location cspawn11 = new Location(world, 150.5, 45, 21.5);
        Location cspawn12 = new Location(world, 78, 34, 3.5);
        Location cspawn13 = new Location(world, 68.5, 33, -16.5);
        Location cspawn14 = new Location(world, 70, 36, 142.5);

        cSpawnL.add(cspawn1);
        cSpawnL.add(cspawn2);
        cSpawnL.add(cspawn3);
        cSpawnL.add(cspawn4);
        cSpawnL.add(cspawn5);
        cSpawnL.add(cspawn6);
        cSpawnL.add(cspawn7);
        cSpawnL.add(cspawn8);
        cSpawnL.add(cspawn9);
        cSpawnL.add(cspawn10);
        cSpawnL.add(cspawn11);
        cSpawnL.add(cspawn12);
        cSpawnL.add(cspawn13);
        cSpawnL.add(cspawn14);

    }

    public Location randomCSpawn(){
        Random random = new Random();
        int randomInt = random.nextInt(cSpawnL.size());
        Location randomLoc = cSpawnL.get(randomInt);

        int yaw = random.nextInt(180);
        int negativePositive = random.nextInt(2);
        if(negativePositive != 1){
            yaw = -yaw;
        }

        randomLoc.setYaw(yaw);
        return randomLoc;
    }
}