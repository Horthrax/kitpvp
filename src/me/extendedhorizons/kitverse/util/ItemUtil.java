package me.extendedhorizons.kitverse.util;

/**
 * Created by horthrax on 12/6/2015.
 */
public class ItemUtil {
    public String getToggleName(String string) {
        String toggleName = "";
        if (string.contains("Toggle")) {
            if (string.contains("off")) {
                toggleName = string.split("Toggle ")[1].split(" off")[0];
                return toggleName;
            }
            if (string.contains("on")) {
                toggleName = string.split("Toggle ")[1].split(" on")[0];
                return toggleName;
            }
        }
        return toggleName;
    }

}