package me.extendedhorizons.kitverse.util;

import me.extendedhorizons.kitverse.classes.Kit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

/**
 * Created by horthrax on 6/26/2015.
 */
public class ArmorSet {
    ItemStack helmet;
    ItemStack chestplate;
    ItemStack leggings;
    ItemStack boots;

    public static void applyClassesArmor(Player player, Kit theKit){
        EntityEquipment equipment = player.getEquipment();
        equipment.setHelmet(theKit.getArmor().getHelmet());
        equipment.setChestplate(theKit.getArmor().getChestplate());
        equipment.setLeggings(theKit.getArmor().getLeggings());
        equipment.setBoots(theKit.getArmor().getBoots());
    }

    //Setters
    public void setHelmet(ItemStack helmet){
        this.helmet = helmet;
    }
    public void setChestplate(ItemStack chestplate){
        this.chestplate = chestplate;
    }
    public void setLeggings(ItemStack leggings){
        this.leggings = leggings;
    }
    public void setBoots(ItemStack boots){
        this.boots = boots;
    }
    //Getters
    public ItemStack getHelmet(){
        return helmet;
    }
    public ItemStack getChestplate(){
        return chestplate;
    }
    public ItemStack getLeggings(){
        return leggings;
    }
    public ItemStack getBoots(){
        return boots;
    }
}
