package me.extendedhorizons.kitverse.util;

import net.minecraft.server.v1_12_R1.EnumSkyBlock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;

/**
 * Created by horthrax on 12/5/2015.
 */
public class BlockUtil {
    public static Location getCenterLoc(Location loc){
        double x = 0;
        double z = 0;
        if(loc.getX() > 0){
            x = 0.5;
        }else{
            x = -0.5;
        }
        if(loc.getZ() > 0){
            z = 0.5;
        }else{
            z = -0.5;
        }
        return loc.add(x,0,z);
    }
    public static boolean hasSurrounding(Block block, Material material) {
        Location loc = block.getLocation().clone();
        return loc.clone().add(1, 0, 0).getBlock().getType().equals(material) && loc.clone().add(0, 0, 1).getBlock()
                .getType().equals(material) && loc.clone().add(-1, 0, 0).getBlock().getType().equals(material) && loc.clone().add(0, 0, -1).getBlock().getType().equals(material);
    }
}