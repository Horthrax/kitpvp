package me.extendedhorizons.kitverse.util;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.settings.SettingTypes;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.classes.loadout.Loadout;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

/**
 * Created by horthrax on 12/6/2015.
 */
public class RespawnHandler implements Listener {
    public Locations locations = new Locations();
    Main main = Main.getInstance();

    public void handleRespawn(Player player) {
        User user = main.getUser(player.getUniqueId());
        Location deathLoc = player.getLocation();
        if (user.getSetting(SettingTypes.AUTO_RESPAWN)) {
            ((CraftServer) Bukkit.getServer()).getHandle().moveToWorld(((CraftPlayer) player).getHandle(), 0, false);
            player.teleport(deathLoc);
            player.getInventory().setHeldItemSlot(4);
            new BukkitRunnable() {
                int count = 6;
                @Override
                public void run() {
                    if (user.getSetting(SettingTypes.AUTO_RESPAWN) && user.getPlayerState().equals(PlayerState.RESPAWN)) {
                        initAutoRespawnInventory(player, count);
                        count--;
                        if (count < 0) {
                            player.teleport(locations.randomCSpawn());
                            Loadout.applyLoadout(player, user.getSelectedKit());
                            user.setPlayerState(PlayerState.PVP);
                            this.cancel();
                        }
                    } else {
                        this.cancel();
                    }
                }
            }.runTaskTimer(main, 1, 20);
        } else {
            user.setPlayerState(PlayerState.SPAWN);
            ((CraftServer) Bukkit.getServer()).getHandle().moveToWorld(((CraftPlayer) player).getHandle(), 0, false);
        }
    }

    public void initAutoRespawnInventory(Player player, int count) {
        //5-14
        ItemStack autoRespawnTrue = new ItemStack(Material.WOOL, count, (byte) 5);
        ItemMeta meta1 = autoRespawnTrue.getItemMeta();
        meta1.setDisplayName(ChatColor.GREEN + "Continue Automatic Respawn");
        autoRespawnTrue.setItemMeta(meta1);

        ItemStack autoRespawnFalse = new ItemStack(Material.WOOL, count, (byte) 14);
        ItemMeta meta2 = autoRespawnTrue.getItemMeta();
        meta2.setDisplayName(ChatColor.DARK_RED + "Cancel Automatic Respawn");
        autoRespawnFalse.setItemMeta(meta2);

        player.getInventory().clear();
        player.getInventory().setItem(0, autoRespawnTrue);
        player.getInventory().setItem(8, autoRespawnFalse);
    }

    @EventHandler
    public void onSlotChange(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        PlayerInventory playerInventory = player.getInventory();
        User user = main.getUser(player.getUniqueId());
        if (user.getSetting(SettingTypes.AUTO_RESPAWN)) {
            if (event.getNewSlot() == 0 || event.getNewSlot() == 8) {
                if (playerInventory.getItem(event.getNewSlot()) != null && !player.getInventory().getItem(event.getNewSlot()).getType().equals(Material.AIR)) {
                    if (playerInventory.getItem(event.getNewSlot()).getType().equals(Material.WOOL)) {
                        if (user.getSelectedKit() != null) {
                            if (playerInventory.getItem(event.getNewSlot()).getData().getData() == 5) {
                                user.setPlayerState(PlayerState.PVP);
                                Loadout.applyLoadout(player, user.getSelectedKit());
                                player.teleport(locations.randomCSpawn());
                                return;
                            }
                            if (playerInventory.getItem(event.getNewSlot()).getData().getData() == 14) {
                                ((CraftServer) Bukkit.getServer()).getHandle().moveToWorld(((CraftPlayer) player).getHandle(), 0, false);
                                player.teleport(locations.spawn);
                                user.setSetting(SettingTypes.AUTO_RESPAWN, false);
                                user.setPlayerState(PlayerState.SPAWN);
                            }
                        }
                    }
                }
            }
        }
    }
}