package me.extendedhorizons.kitverse.shrines;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by horthrax in KitVerse on 12/31/2015.
 */
public class Shrine {

    Main main = Main.getInstance();

    Location location;
    String name;
    ChatColor nameColor;
    PotionEffectType potionType;
    int duration;
    int amplifier;

    public Shrine(Location loc, String name, ChatColor nameColor, PotionEffectType potionType, int duration, int amplifier){
        this.location = loc;
        this.name = name;
        this.nameColor = nameColor;
        this.potionType = potionType;
        this.amplifier = amplifier;
        this.duration = duration;

        loc.getWorld().spawnEntity(loc,EntityType.ENDER_CRYSTAL);

        main.shrines.add(this);
    }

    public Location getLocation(){
        return this.location;
    }

    public String getName(){
        return this.name;
    }

    public ChatColor getNameColor(){
        return this.nameColor;
    }

    public PotionEffectType getPotionType(){
        return this.potionType;
    }

    public void respawn(Shrine shrine, int delayTicks){
        new BukkitRunnable(){
            @Override
            public void run(){
                shrine.location.getWorld().spawnEntity(shrine.location,EntityType.ENDER_CRYSTAL);
            }
        }.runTaskLater(shrine.main,delayTicks);
    }

    public void activateShrine(Player player){
        User user = main.getUser(player.getUniqueId());
        Rank rank = user.getRank();
        PotionEffect potionEffect = new PotionEffect(potionType, duration, amplifier, false);
        player.addPotionEffect(potionEffect);
        for(Entity entity : Bukkit.getWorld("world").getEntities()){
            if(entity.getType().equals(EntityType.ENDER_CRYSTAL) && entity.getLocation().equals(this.location)){
                entity.remove();
            }
        }
        if(name.equalsIgnoreCase("red")){
            player.setHealth(player.getMaxHealth());
        }
        Bukkit.broadcastMessage(rank.getPrefix() + rank.getUsernameColor() + player.getName() + ChatColor.AQUA + " collected the " + this.nameColor + this.name + ChatColor.AQUA + " shrine!");
        respawn(this, 20 * 60 * 10); //Respawn in 10 minutes
    }
}
