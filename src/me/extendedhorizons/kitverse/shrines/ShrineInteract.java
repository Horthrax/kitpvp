package me.extendedhorizons.kitverse.shrines;

import me.extendedhorizons.kitverse.Main;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/**
 * Created by horthrax in KitVerse on 12/31/2015.
 */
public class ShrineInteract implements Listener{
    Main main = Main.getInstance();

    @EventHandler
    public void interactWithShrine(PlayerInteractEntityEvent event){
        Player player = event.getPlayer();
        if(event.getRightClicked().getType().equals(EntityType.ENDER_CRYSTAL)){
            for(Shrine shrines : main.shrines) {
                if (shrines.getLocation().equals(event.getRightClicked().getLocation())){
                    shrines.activateShrine(player);
                }
            }
        }
    }
}
