package me.extendedhorizons.kitverse;

import me.extendedhorizons.kitverse.blacksmiths.Blacksmith;
import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.cosmetics.Leaves;
import me.extendedhorizons.kitverse.cosmetics.chattab.Tab;
import me.extendedhorizons.kitverse.economy.EconomyType;
import me.extendedhorizons.kitverse.economy.OperatorType;
import me.extendedhorizons.kitverse.inventories.PlayerStats;
import me.extendedhorizons.kitverse.inventories.SettingsGui;
import me.extendedhorizons.kitverse.tutorial.TutorialVillager;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.donators.DonatorPerks;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import me.extendedhorizons.kitverse.user.settings.SettingTypes;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import me.extendedhorizons.kitverse.util.Locations;
import me.extendedhorizons.kitverse.util.RespawnHandler;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

/**
 * Created by horthrax in KitVerse on 6/26/2015.
 */
@SuppressWarnings("unused")
public class Events implements Listener {
    Main main = Main.getInstance();

    private RankData rankData = new RankData();

    private Locations locations = new Locations();
    private RespawnHandler respawnHandler = new RespawnHandler();
    private Kit kit = main.kits;
    private Blacksmith blacksmith = main.blacksmith;

    Tab tab = new Tab();
    Leaves leaves = main.leaves;
    EconomyHandler economyHandler = new EconomyHandler();

    String noperms = ChatColor.DARK_RED + "Sorry," + ChatColor.RED + " you don't have permission to do this.";

    @EventHandler
    public void join(PlayerJoinEvent event) {
        String bold = ChatColor.BOLD + "";
        Player player = event.getPlayer();
        CraftPlayer craftPlayer = (CraftPlayer) player;
        main.getDB().addPlayer(player);
        User user = new User(player.getUniqueId(),player.getName());
        Rank rank = user.getRank();
        kit.profiles.put(player, craftPlayer.getProfile());
        kit.loadSpawnKits(player);
        blacksmith.loadBlacksmiths(player);
        player.teleport(locations.spawn);
        player.setGameMode(GameMode.SURVIVAL);
        for (int i = 0; i <= 251; i++) {
            player.sendMessage(" ");
        }
        player.setFoodLevel(19);
        PlayerStats stats = new PlayerStats();
        DonatorPerks station = new DonatorPerks();
        SettingsGui settings = new SettingsGui();
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().setItem(3, settings.settingsItemStack());
        player.getInventory().setItem(4, stats.compassStatsItem());
        player.getInventory().setItem(5, station.donationStationStack());
        player.getInventory().setHeldItemSlot(4);
        player.setHealth(20);
        //player.setExp(0);
        player.setTotalExperience(0);
        player.getActivePotionEffects().clear();
        TutorialVillager.loadVillager(player);

        if (!player.hasPlayedBefore()) {
            event.setJoinMessage(ChatColor.AQUA + "[" + ChatColor.GREEN + "+" + ChatColor.AQUA + "] " + ChatColor.DARK_GRAY.toString() + rank.getPrefix() + rank.getUsernameColor().toString() + player.getName() + " §3has joined for the first time!");
        }else {
            event.setJoinMessage(ChatColor.AQUA + "[" + ChatColor.GREEN + "+" + ChatColor.AQUA + "] " + rank.getPrefix() + rank.getUsernameColor().toString() + player.getName());
        }

        /*
        Originally 5 ='s now 6 more
        27 chars in the divider
        20 in KitPvp at its finest
        These are non staff commands
        */
        player.sendMessage(
                "            §f§k§l:§f§k§l|§3§lKit§b§lVerse§f§k§l:§f§k§l: \n" +
                "§3--===========+===========--\n" + //27
                        "        §cWelcome to KitVerse!\n" +  //20/2 = 10
                        "If you're new be sure to right click\n" +  //29/2 = 14.5
                        "       the tutorial villager.\n" +  //24/2 = 12
                        "    If you need any help contact\n" +  //28/2 = 14
                        "           a staff member.\n" +  //15/2 = 7.5
                "§3--===========+===========--\n");  //27
        player.sendMessage("\n");
        tab.initTab(player);
    }

    @EventHandler
    public void stopFood(FoodLevelChangeEvent event) {
        if (event.getFoodLevel() == 19) {
            event.setCancelled(true);
        }
        event.setFoodLevel(19);
    }

    @EventHandler
    public void leave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        User user = main.getUser(player.getUniqueId());
        user.setPlayerState(PlayerState.OFFLINE);
        if (kit.profiles.containsKey(player)) {
            kit.profiles.remove(player);
        }
        kit.clearKits(player);
        Rank rank = user.getRank();
        event.setQuitMessage(ChatColor.AQUA + "[" + ChatColor.RED + "-" + ChatColor.AQUA + "] " + rank.getPrefix() + rank.getUsernameColor().toString() + player.getName());
        user.updateDatabase();
    }

    @EventHandler
    public void loadServer(ServerListPingEvent event) {
        String bold = ChatColor.BOLD.toString();
        String motd1 = "                        " + ChatColor.WHITE + "" + ChatColor.MAGIC + bold + "ii" + ChatColor.DARK_AQUA + bold + "Kit" + ChatColor.AQUA + bold + "Verse" + ChatColor.WHITE + "" + ChatColor.MAGIC + bold + "ii";
        String motd2 = ChatColor.WHITE + "                » " + ChatColor.RED + "Conquer The Verse!" + ChatColor.WHITE + " «";

        event.setMaxPlayers(250);
        event.setMotd(motd1 + "\n" + motd2);
    }

    @EventHandler
    public void stopRain(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void playerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        User user = main.getUser(player.getUniqueId());
        new BukkitRunnable() {
            @Override
            public void run() {
                Locations location = new Locations();
                event.getPlayer().teleport(locations.spawn);
                TutorialVillager.respawn(player.getUniqueId());
                kit.clearKits(player);
                blacksmith.clearBlacksmith(player);

                kit.loadSpawnKits(player);
                blacksmith.loadBlacksmiths(player);

                if (!user.getSetting(SettingTypes.AUTO_RESPAWN)) {
                    PlayerStats stats = new PlayerStats();
                    DonatorPerks station = new DonatorPerks();
                    SettingsGui settings = new SettingsGui();

                    player.getInventory().clear();
                    player.getInventory().setItem(3, settings.settingsItemStack());
                    player.getInventory().setItem(4, stats.compassStatsItem());
                    player.getInventory().setItem(5, station.donationStationStack());
                }
                player.setFoodLevel(19);
            }
        }.runTaskLater(main, 1);
    }

    /**
     * The following code is black magic and will break if you look at it funny, do not touch.
     */

    @EventHandler
    public void playerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        ArrayList<String> deathMessages = new ArrayList<>();
        deathMessages.add("demolished");
        deathMessages.add("destroyed");
        deathMessages.add("decapitated");
        deathMessages.add("slain");
        deathMessages.add("assassinated");
        Random random = new Random();

        String randomDeathMessage = deathMessages.get(random.nextInt(deathMessages.size()));
        UUID killedUUID = player.getUniqueId();
        User user = main.getUser(killedUUID);
        user.setPlayerState(PlayerState.RESPAWN);

        if (event.getEntity().getKiller() != null) {
            Player killer = player.getKiller().getPlayer();
            UUID killerUUID = killer.getUniqueId();
            User killerUser = main.getUser(killerUUID);
            if (event.getEntity().getKiller() != null && event.getEntity().getKiller() != event.getEntity()) {
                killerUser.updateKills(1);
                user.updateDeaths(1);
                String killerUserKDR = user.getSetting(SettingTypes.SHOW_KDR) ? ChatColor.DARK_AQUA + " [" + ChatColor.AQUA + killerUser.getKDR() + ChatColor.DARK_AQUA + "]" : "";
                String userKDR = user.getSetting(SettingTypes.SHOW_KDR) ? ChatColor.DARK_AQUA + " [" + ChatColor.AQUA + user.getKDR() + ChatColor.DARK_AQUA + "] " : " ";
                event.setDeathMessage(ChatColor.AQUA + player.getName() + userKDR + ChatColor.GRAY + "was " + randomDeathMessage + " by " + ChatColor.AQUA + killer.getName() + killerUserKDR + ChatColor.GRAY + ".");
                if(killedUUID != null && killerUUID != null) {
                    economyHandler.updateEconomy(EconomyType.BOTH, EconomyHandler.EconomyCause.KILLED_PLAYER,killerUUID,killedUUID, 0);
                    economyHandler.updateEconomy(EconomyType.BOTH,OperatorType.ADD,killerUUID,10);
                }
                event.getDrops().clear();
                Location deathLoc = event.getEntity().getLocation();
                new BukkitRunnable(){
                    @Override
                    public void run(){
                        killer.sendMessage(ChatColor.GOLD + "+" + "4" + ChatColor.YELLOW + " crystals.");
                        killer.sendMessage(ChatColor.DARK_AQUA + "+" + "6" + ChatColor.AQUA + " orbs.");
                    }
                }.runTaskLater(main,1);
                respawnHandler.handleRespawn(player);
                return;
            }
        }
        user.updateDeaths(1);
        String userKDR = user.getSetting(SettingTypes.SHOW_KDR) ? ChatColor.DARK_AQUA + " [" + ChatColor.AQUA + user.getKDR() + ChatColor.DARK_AQUA + "] " : " ";
        event.setDeathMessage(ChatColor.AQUA + player.getName() + ChatColor.DARK_AQUA + userKDR + ChatColor.GRAY + "was " + randomDeathMessage + ".");
        event.getDrops().clear();
        economyHandler.updateEconomy(EconomyType.BOTH, EconomyCause.DEATH, killedUUID, null, 0);
        respawnHandler.handleRespawn(player);
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        if (event.getPlayer().getGameMode().equals(GameMode.SURVIVAL)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void blockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode().equals(GameMode.SURVIVAL)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void pvpStop(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            User user = main.getUser(player.getUniqueId());
            if (player.getGameMode().equals(GameMode.CREATIVE) || !user.getPlayerState().equals(PlayerState.PVP) && !user.getPlayerState().equals(PlayerState.ABILITY)) {
                event.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void pvpStop(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            User user = main.getUser(player.getUniqueId());
            if (player.getGameMode().equals(GameMode.CREATIVE) || !user.getPlayerState().equals(PlayerState.PVP) && !user.getPlayerState().equals(PlayerState.ABILITY)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void stopDrops(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.SURVIVAL) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void manageSpawns(EntitySpawnEvent event) {
        if (event.getEntity() instanceof LivingEntity && !(event.getEntity() instanceof HumanEntity || event.getEntity() instanceof ArmorStand)) {
            main.getLogger().info("Blocking spawn for " + event.getEntity().getType().name() + " because it wasn't a player!");
            event.setCancelled(true);
        }
    }
    @EventHandler
    public void lowestLeafBlockEvent(BlockBreakEvent event){
        Player player = event.getPlayer();
        if(player.getGameMode().equals(GameMode.CREATIVE) && player.getItemInHand().getType().equals(Material.LADDER)){
            if(event.getBlock().getType().equals(Material.LEAVES)){
                event.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void stopClick(InventoryClickEvent event){
        Player player = (Player) event.getWhoClicked();
        if(!(player.getGameMode() == GameMode.CREATIVE && rankData.getPowerLevel(player.getUniqueId()) >=4)){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void stopPickup(PlayerPickupItemEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void helpCommand(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (event.getMessage().contains("/help")) {
            if(!rankData.isStaff(player.getUniqueId())) {
                player.sendMessage(
                        "     §f§k§l:§f§k§l|§3§lKit§b§lVerse§f§k§l:§f§k§l: \n" +
                                "§3--======+======--\n" +
                                "       §6Commands:\n" +
                                "         §e/orbs\n" +
                                "         §e/crystals\n" +
                                "         §e/balance [bal]\n" +
                                "§3--======+======--");
                event.setCancelled(true);
            }else{
                player.sendMessage(
                        "     §f§k§l:§f§k§l|§3§lKit§b§lVerse§f§k§l:§f§k§l: \n" +
                                "§3--======+======--\n" +
                                "       §6Commands:\n" +
                                "         §e/setrank\n" +
                                "         §e/clearchat [cc]\n" +
                                "         §e/settier\n" +
                                "         §e/resettiers\n" +
                                "         §e/updateorbs\n" +
                                "         §e/updatecrystals\n" +
                                "         §e/orbs\n" +
                                "         §e/crystals\n" +
                                "         §e/balance [bal]\n" +
                                "         §e/setkills\n" +
                                "         §e/setdeaths\n" +
                                "         §e/gmc\n" +
                                "         §e/gms\n" +
                                "         §e/gmsp\n" +
                                "         §e/gma\n" +
                                "§3--======+======--");
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void itemDamage(PlayerItemDamageEvent event){
        event.setCancelled(true);
        event.getPlayer().updateInventory();
    }

    @EventHandler
    public void steakHeal(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();
        if (event.getItem().getType().equals(Material.COOKED_BEEF)) {

            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 7, 1));
            player.setFoodLevel(19);
        }
    }

    @EventHandler
    public void explodeEvent(EntityExplodeEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void farmLandStop(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction() == Action.PHYSICAL) {
            Block block = event.getClickedBlock();
            if (block == null) return;
            if (block.getType() == Material.SOIL) {
                event.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
                event.setCancelled(true);
                block.setTypeIdAndData(block.getType().getId(), block.getData(), true);
            }
        }
    }

    @EventHandler
    public void fixShit(ProjectileHitEvent event){
        if(event.getEntity() instanceof Arrow) {
            main.arrows.remove(event.getEntity());
            event.getEntity().remove();
        }
    }

    @EventHandler
    public void playerMoveEvent(PlayerMoveEvent event){
        Player player = event.getPlayer();
        if(player.getLocation().getY() >= 56 && !player.getGameMode().equals(GameMode.CREATIVE)){
            Location higherLoc = player.getLocation().clone();
            higherLoc.setY(60);
            //3x3
            Location loc1 = higherLoc.clone().add(0,0,0);
            //
            Location loc2 = higherLoc.clone().add(0,0,1);
            Location loc3 = higherLoc.clone().add(1,0,1);
            Location loc4 = higherLoc.clone().add(1,0,0);
            Location loc5 = higherLoc.clone().add(1,0,-1);
            Location loc6 = higherLoc.clone().add(0,0,-1);
            Location loc7 = higherLoc.clone().add(-1,0,-1);
            Location loc8 = higherLoc.clone().add(-1,0,0);
            Location loc9 = higherLoc.clone().add(0,0,1);

            //Surrounding
            Location surrounding1 = higherLoc.clone().add(-1,0,2);
            Location surrounding2 = higherLoc.clone().add(0,0,2);
            Location surrounding3 = higherLoc.clone().add(1,0,2);
            //
            Location surrounding4 = higherLoc.clone().add(2,0,1);
            Location surrounding5 = higherLoc.clone().add(2,0,0);
            Location surrounding6 = higherLoc.clone().add(2,0,-1);
            //
            Location surrounding7 = higherLoc.clone().add(1,0,-2);
            Location surrounding8 = higherLoc.clone().add(0,0,-2);
            Location surrounding9 = higherLoc.clone().add(-1,0,-2);
            //
            Location surrounding10 = higherLoc.clone().add(-2,0,1);
            Location surrounding11 = higherLoc.clone().add(-2,0,0);
            Location surrounding12 = higherLoc.clone().add(-2,0,-1);
            /*
            0,0,1;
            1,0,1;
            1,0,0;
            1,0,-1;
            0,0,-1;
            -1,0,-1;
            -1,0,0;
            -1,0,1;
            //surrounding
            -1,0,2;
            0,0,2;
            1,0,2;
            //
            2,0,1;
            2,0,0;
            2,0,-1;
            //
            1,0,-2;
            0,0,-2;
            -1,0,-2;
            //
            -2,0,1;
            -2,0,0;
            -2,0,-1;
            */
        }
    }

}