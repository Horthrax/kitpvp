package me.extendedhorizons.kitverse.cosmetics.chattab;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.states.PlayerState;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by horthrax on 7/1/2015.
 */
public class ChatFormatting implements Listener {
    Main main = Main.getInstance();
    @EventHandler
    public void chatFormat(final AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        //FORMATTING
        Rank rank = main.getUser(player.getUniqueId()).getRank();
        event.setFormat(rank.getPrefix() + rank.getUsernameColor().toString() + event.getPlayer().getName() + ChatColor.LIGHT_PURPLE.toString() + " » " + rank.getChatColor().toString() + event.getMessage());
        for(Player players : event.getRecipients()){
            User users = main.getUser(players.getUniqueId());
            if(users.getPlayerState().equals(PlayerState.TUTORIAL)){
                event.getRecipients().remove(players);
            }
        }
    }
}