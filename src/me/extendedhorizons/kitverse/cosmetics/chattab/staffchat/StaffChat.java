package me.extendedhorizons.kitverse.cosmetics.chattab.staffchat;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.RankData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by horthrax in KitVerse on 1/1/2016.
 */
public class StaffChat implements Listener{
    RankData rankData = new RankData();

    @EventHandler
    public void chatEvent(AsyncPlayerChatEvent event){
        Player player = event.getPlayer();
        String message = event.getMessage();
        if(message != null) {
            if (message.toLowerCase().startsWith("!") && rankData.isStaff(player.getUniqueId())) {
                if(message.toLowerCase().startsWith("! ")){
                    rankData.sendStaffChatMessage(player.getUniqueId(), event.getMessage());
                }else if(message.toLowerCase().startsWith("!")){
                    rankData.sendStaffChatMessage(player.getUniqueId(), event.getMessage());
                }
                event.setCancelled(true);
            }
        }
    }
}