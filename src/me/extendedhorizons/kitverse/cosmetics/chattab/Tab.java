package me.extendedhorizons.kitverse.cosmetics.chattab;

import com.google.gson.stream.JsonReader;
import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.Reader;
import java.lang.reflect.Field;

/**
 * Created by horthrax on 10/20/2015.
 */
public class Tab {
    Main main = Main.getInstance();
    public void setTab(Player p,String headerText, String msgText) {
        CraftPlayer pl = (CraftPlayer) p;
        PlayerConnection c = pl.getHandle().playerConnection;
        IChatBaseComponent header = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + headerText + "\"}");

        IChatBaseComponent msg = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msgText + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try {
            Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, header);
            headerField.setAccessible(!headerField.isAccessible());

            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, msg);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.sendPacket(packet);
        }
    }

    public void setPlayerListName(Player player){
        new BukkitRunnable() {
            @Override
            public void run(){
                User user = main.getUser(player.getUniqueId());
                Rank rank = user.getRank();
                player.setPlayerListName(rank.getPrefix()+rank.getUsernameColor().toString() +player.getName());
                for(Player onlinePlayers :Bukkit.getOnlinePlayers()) {
                    User onlineUsers = main.getUser(onlinePlayers.getUniqueId());
                    Rank onlineRanks = onlineUsers.getRank();
                    onlinePlayers.setPlayerListName(onlineRanks.getPrefix() + onlineRanks.getUsernameColor().toString() + onlinePlayers.getName());
                }
            }
        }.runTaskLater(main,2);
    }
    public void initTab(Player player){
        String bold = ChatColor.BOLD.toString();
        setPlayerListName(player);
        setTab(player, ChatColor.WHITE + "" + ChatColor.MAGIC + bold + "ii" + ChatColor.DARK_AQUA + bold + "Kit" + ChatColor.AQUA + bold + "Verse" + ChatColor.WHITE + "" + ChatColor.MAGIC + bold + "ii", "§f»» " + "§ckitverse.co §f««");
    }
}
