package me.extendedhorizons.kitverse.cosmetics.chattab;

import me.extendedhorizons.kitverse.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by horthrax in KitVerse on 1/6/2016.
 */
public class Announcements {
    Main main = Main.getInstance();
    Random rand = new Random();
    ArrayList<String> announcements = new ArrayList<>();
    public Announcements(){
        announcements.add("Be sure to tell your friends about us!");
        announcements.add("Need Help? Contact a staff member.");
        announcements.add("Visit our website http://web.kitverse.co");
        announcements.add("Get awesome perks at http://web.kitverse.co");
        announcements.add("Be sure to vote now with /vote.");
        announcements.add("Want to get some orbs? Vote now /vote!");
        new BukkitRunnable(){
            @Override
            public void run(){
                int random = rand.nextInt(announcements.size());
                String randomAnouncement = announcements.get(random);
                Bukkit.broadcastMessage(ChatColor.RED.toString() + ChatColor.BOLD + "Info " + ChatColor.DARK_RED.toString() + ChatColor.BOLD + "»» " + ChatColor.DARK_AQUA + randomAnouncement);
            }
        }.runTaskTimer(main,1,20*30*5); //20*30*5
    }
}
