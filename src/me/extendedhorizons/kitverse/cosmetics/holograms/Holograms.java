package me.extendedhorizons.kitverse.cosmetics.holograms;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 6/28/2015.
 */
public class Holograms {
    World world = Bukkit.getWorld("world");
    public List<ArmorStand> hologramsList = new ArrayList<>();

    public Holograms() {
        createHologram(new Location(world,52.500,97.5,51.0),"Tier I",ChatColor.DARK_PURPLE, ChatColor.LIGHT_PURPLE, "><");
        createHologram(new Location(world,46.500,97.5,57.5),"Tier II",ChatColor.DARK_PURPLE, ChatColor.LIGHT_PURPLE, "><");
        createHologram(new Location(world,59.500,97.5,57.5),"Tier III",ChatColor.DARK_PURPLE, ChatColor.LIGHT_PURPLE, "><");
        createHologram(new Location(world,54.0,96,62.0),"Tutorial",ChatColor.DARK_AQUA, ChatColor.AQUA, "><");
    }

    public void createHologram(Location loc, String name, ChatColor primaryColor, ChatColor secondaryColor, String border){
        ArmorStand hologram = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        String[] borders = new String[2];
        borders[0] = border.substring(0, border.length() / 2);
        borders[1] = border.substring(border.length() / 2, border.length());
        hologram.setCustomName(secondaryColor + borders[0] + primaryColor + name + secondaryColor + borders[1]);
        hologram.setCustomNameVisible(true);
        hologram.setBasePlate(false);
        hologram.setSmall(true);
        hologram.setGravity(false);
        hologram.setVisible(false);
        hologramsList.add(hologram);
    }

    public void clearHolograms() {
        for (Entity entity : hologramsList) {
            entity.remove();
        }
    }
}
