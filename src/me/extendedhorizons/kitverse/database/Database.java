package me.extendedhorizons.kitverse.database;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.economy.EconomyType;
import me.extendedhorizons.kitverse.user.ranks.Rank;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.*;
import java.util.UUID;

/**
 * Created by horthrax on 7/1/2015.
 */
@SuppressWarnings("unused")
public class Database {
    Main main = Main.getInstance();
    private Connection connection;

    public ResultSet query(String query, Object... args) {
        try {
            getConnection();
            PreparedStatement statement = eval(query, args);
            return statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void executeUpdate(String query, Object... args) {
        try {
            getConnection();
            PreparedStatement statement = eval(query, args);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement eval(String query, Object... args) {
        if (StringUtils.countMatches(query, "?") != args.length) {
            throw new IllegalArgumentException("Query placeholders not equal to arguments length");
        }

        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof String) {
                    statement.setString(i + 1, (String) args[i]);
                }
                if (args[i] instanceof Integer) {
                    statement.setInt(i + 1, (Integer) args[i]);
                }
                if (args[i] instanceof Boolean) {
                    statement.setBoolean(i + 1, (Boolean) args[i]);
                }
                if (args[i] instanceof Long) {
                    statement.setLong(i + 1, (Long) args[i]);
                } else {
                    statement.setObject(i + 1, args[i]);
                }
            }
            return statement;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void initialize() {
        System.out.println("Initializing database tables!");
        executeUpdate("CREATE TABLE IF NOT EXISTS kit_verse (id integer, uuid varchar(255), username varchar(255), kills integer, deaths integer, archer_tier integer, warrior_tier integer, brewer_tier integer, thief_tier integer, mage_tier integer, rank varchar(255), best_killstreak integer, experience integer, balance double);");
    }

    public void getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                connect();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        try {
            java.lang.Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kit_verse?autoReconnect=true&useSSL=false", "root", "password");

            //"jdbc:mysql://localhost:3306/root?autoReconnect=true", "", ""
        } catch (SQLException e) {
            Bukkit.broadcastMessage("Connection Failed! Check output console");
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkExists(UUID uuid) {
        try {
            ResultSet rs = query("SELECT uuid FROM kit_verse WHERE uuid = ?", uuid.toString());
            if(rs.next()) {
                rs.close();
                return true;
            }else{
                rs.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean checkExists(String userName) {
        try {
            ResultSet rs = query("SELECT username FROM kit_verse WHERE username = ?", userName);
            if(rs.next()) {
                rs.close();
                return true;
            }else{
                rs.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private int getLastId(){
        ResultSet rs = query("SELECT id FROM kit_verse ORDER BY id DESC LIMIT 1");
        try{
            if(rs.next()) {
                return rs.getInt("id");
            }else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void addPlayer(Player player) {
        main.getLogger().info(player.getUniqueId().toString());
        boolean exists = checkExists(player.getUniqueId());
        main.getLogger().info("Player Exists: " + exists);

        if(!exists) {
            main.getLogger().info("Player wasn't in database.");
            executeUpdate("INSERT INTO kit_verse (id, uuid, username, kills, deaths, archer_tier, warrior_tier, brewer_tier, thief_tier, mage_tier, rank, best_killstreak, orbs, crystals) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?)", (getLastId() + 1), player.getUniqueId().toString(), player.getName(), 0, 0, 1, 1, 1, 1, 1, "default", 0, 0.0, 0.0);
            main.getLogger().info("Successfully added player " + player.getName() + " (" + player.getUniqueId().toString() + ") to db.");
        }
    }
    public void setRankExact(UUID uuid, Rank rank) {
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET rank = ? WHERE uuid = ?", rank.name().toLowerCase(), uuid.toString());
        }else{
            main.getUser(uuid).rank = rank;
        }
    }
    public void updateUserName(UUID uuid, String userName) {
        executeUpdate("UPDATE kit_verse SET username = ? WHERE uuid = ?", userName.toLowerCase(), uuid.toString());

    }
    /*
        Bob #1 ~ Changes name and doesnt join ever again
        Bob #2 ~ joins server and trys to update username, but the username is already existant in the database
     */
    public void setTierExact(UUID uuid, Tier tier, int tierNumber){
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET " + tier.name().toLowerCase() + " = ? WHERE uuid = ?", tierNumber, uuid.toString());
        }else{
            main.getUser(uuid).tierData.put(tier,tierNumber);
        }
    }
    public void setBalanceExact(UUID uuid, double amount){
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET balance = balance + ? WHERE uuid = ?", amount, uuid.toString());
        }else{
            main.getUser(uuid).crystals += amount;
        }
    }
    public void setExperienceExact(UUID uuid, double amount){
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET experience = experience + ? WHERE uuid = ?", amount, uuid.toString());
        }else{
            main.getUser(uuid).orbs += amount;
        }
    }
    public void setDeathsExact(UUID uuid, int amount){
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET deaths = deaths + ? WHERE uuid = ?", amount, uuid.toString());
        }else{
            main.getUser(uuid).setDeaths(amount);
        }
    }
    public void setKillsExact(UUID uuid, int amount){
        if(main.getUser(uuid) == null) {
            executeUpdate("UPDATE kit_verse SET kills = kills + ? WHERE uuid = ?", amount, uuid.toString());
        }else{
            main.getUser(uuid).setKills(amount);
        }
    }
    public void resetTiersExact(UUID uuid){
        setTierExact(uuid,Tier.ARCHER_TIER,1);
        setTierExact(uuid,Tier.WARRIOR_TIER,1);
        setTierExact(uuid,Tier.BREWER_TIER,1);
        setTierExact(uuid,Tier.MAGE_TIER,1);
        setTierExact(uuid,Tier.THIEF_TIER,1);
    }
    //Offline Getters
    @Deprecated
    public OfflinePlayer getPlayerExact(UUID uuid){
        return Bukkit.getOfflinePlayer(uuid);
    }
    public int getKillsExact(UUID uuid){
        if(main.getUser(uuid) == null) {
            ResultSet rs = main.database.query("SELECT kills FROM kit_verse WHERE uuid = ?", uuid.toString());
            try {
                if (rs.next()) {
                    return rs.getInt("kills");
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return main.getUser(uuid).getKills();
        }
    }
    public int getDeathsExact(UUID uuid) {
        if(main.getUser(uuid) == null) {
            ResultSet rs = query("SELECT deaths FROM kit_verse WHERE uuid = ?", uuid.toString());
            try {
                if (rs.next()) {
                    return rs.getInt("deaths");
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return main.getUser(uuid).getDeaths();
        }
    }
    public Rank getRankExact(UUID uuid) {
        if(Bukkit.getPlayer(uuid) == null) {
            ResultSet rs = query("SELECT rank FROM kit_verse WHERE uuid = ?", uuid.toString());
            try {
                if (rs.next()) {
                    return Rank.getByName(rs.getString("rank"));
                } else {
                    return Rank.DEFAULT;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return Rank.DEFAULT;
            }
        }else{
            return main.getUser(uuid).getRank();
        }
    }
    public int getTier(Tier tier, UUID uuid) {
        if(main.getUser(uuid) == null) {
            ResultSet rs = query("SELECT " + tier.getName().toLowerCase() + " FROM kit_verse WHERE uuid = ?", uuid.toString());
            try {
                if (rs.next()) {
                    return rs.getInt(tier.getName().toLowerCase());
                } else {
                    return 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
        }else{
            return main.getUser(uuid).getTier(tier);
        }
    }
    public int getCurrency(EconomyType type, UUID uuid) {
        ResultSet rs = main.getDB().query("SELECT " + type.name().toLowerCase() + " FROM kit_verse WHERE uuid = ?", uuid.toString());
        int retVal = 0;
        try {
            if (rs.next()) {
                retVal = rs.getInt(type.name().toLowerCase());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    public double getKDR(UUID uuid) {
        if(main.getUser(uuid) == null) {
            if (getKillsExact(uuid) > 0) {
                if (getDeathsExact(uuid) == 0) {
                    return getKillsExact(uuid);
                } else {
                    return (Math.round(((double) getKillsExact(uuid) / (double) getDeathsExact(uuid)) * 100) / (double) 100);
                }
            } else {
                return 0;
            }
        }else{
            return main.getUser(uuid).getKDR();
        }
    }

    @Deprecated
    public UUID getUUID(String userName) {
        if (this.checkExists(userName)) {
            if (main.getUser(userName) == null) {
                ResultSet rs = query("SELECT uuid FROM kit_verse WHERE username = ?", userName.toLowerCase());
                try {
                    if (rs.next()) {
                        return UUID.fromString(rs.getString("uuid"));
                    }
                    return null;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return main.getUser(userName).getUUID();
        } else {
            try {
                URL url = new URL(String.format("https://api.mojang.com/users/profiles/minecraft/%s", userName));
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String tmp;
                String result = "";
                while ((tmp = in.readLine()) != null) {
                    result += tmp;
                }
                if (result.contains("\"id\":")) {
                    String id = result.split("id\":\"")[1].split("\",\"")[0];
                    return UUID.fromString(id.replaceFirst("([0-9a-fA-F]{8})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]{4})([0-9a-fA-F]+)", "$1-$2-$3-$4-$5"));
                }
                main.getLogger().info("Error, something went wrong, couldn't fetch profile for User " + userName + "!");
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}