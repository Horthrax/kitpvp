package me.extendedhorizons.kitverse.blacksmiths;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.user.User;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by horthrax on 6/28/2015.
 */
public class BlacksmithInventory {
    Main main = Main.getInstance();
    public BlacksmithInventory(Player player){
        Inventory inventory = Bukkit.createInventory(null,54, ChatColor.DARK_GRAY + "Blacksmith" + ChatColor.BLACK + "  ⚒");

        ItemStack bowStack = new ItemStack(Material.BOW);
        ItemMeta bowMeta = bowStack.getItemMeta();
        bowMeta.setDisplayName(ChatColor.GRAY + "Archer Tier");
        bowStack.setItemMeta(bowMeta);

        ItemStack axeStack = new ItemStack(Material.WOOD_AXE);
        ItemMeta axeMeta = axeStack.getItemMeta();
        axeMeta.setDisplayName(ChatColor.DARK_RED + "Warrior Tier");
        axeStack.setItemMeta(axeMeta);

        ItemStack standStack = new ItemStack(Material.BREWING_STAND_ITEM);
        ItemMeta standMeta = standStack.getItemMeta();
        standMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Brewer Tier");
        standStack.setItemMeta(standMeta);

        ItemStack stickStack = new ItemStack(Material.STICK);
        ItemMeta stickMeta = stickStack.getItemMeta();
        stickMeta.setDisplayName(ChatColor.GOLD + "Mage Tier");
        stickStack.setItemMeta(stickMeta);

        ItemStack featherStack = new ItemStack(Material.FEATHER);
        ItemMeta featherMeta = featherStack.getItemMeta();
        featherMeta.setDisplayName(ChatColor.RED + "Thief Tier");
        featherStack.setItemMeta(featherMeta);
        /*
        //First row
        inventory.setItem(11,bowStack);
        inventory.setItem(12,axeStack);
        inventory.setItem(13,standStack);
        inventory.setItem(14,stickStack);
        inventory.setItem(15,featherStack);
        //Second row
        inventory.setItem(20,prestigeInfo());
        inventory.setItem(21,prestigeInfo());
        inventory.setItem(22,prestigeInfo());
        inventory.setItem(23,prestigeInfo());
        inventory.setItem(24,prestigeInfo());
        //Third row
        inventory.setItem(39,experienceStack(player));
        inventory.setItem(40,placeHolder());
        inventory.setItem(41,balanceStack(player));
        */


        inventory.all(blankSpace());
        inventory.setItem(11, bowStack);
        inventory.setItem(12, axeStack);
        inventory.setItem(13, standStack);
        inventory.setItem(14, stickStack);
        inventory.setItem(15, featherStack);
        inventory.setItem(20, prestigeInfo());
        inventory.setItem(21, prestigeInfo());
        inventory.setItem(22, prestigeInfo());
        inventory.setItem(23, prestigeInfo());
        inventory.setItem(24, prestigeInfo());
        inventory.setItem(39, experienceStack(player));
        inventory.setItem(40, placeHolder());
        inventory.setItem(41, balanceStack(player));

        player.openInventory(inventory);
    }
    public static void openTierGui(Player player, String itemName, Tier tier, int tierNumber, ItemStack stack) {
        ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE,1,(byte) 15);
        ItemMeta meta = blank.getItemMeta();
        meta.setDisplayName(" ");
        blank.setItemMeta(meta);

        player.closeInventory();
        StringBuilder builder = new StringBuilder();
        String sentence = tier.getName().replaceAll("_"," ");
        for (String word : sentence.split(" ")){
            builder.append(StringUtils.capitalize(word)).append(" ");
        }
        String name = builder.toString();
        Inventory inventory = Bukkit.createInventory(null, 27, name);
        ArrayList<ItemStack> stacks = tierStacks(stack, itemName, tier, tierNumber);
        for(int i = 0; i <= 26; i++) {
            if(i == 11){
                inventory.setItem(i, stacks.get(0));
            }else if(i == 13){
                inventory.setItem(i, stacks.get(1));
            }else if(i == 15){
                inventory.setItem(i, stacks.get(2));
            }else{
                inventory.setItem(i, blank);
            }
        }
        player.openInventory(inventory);
    }

    public ItemStack prestigeInfo(){
        ItemStack placeHolder = new ItemStack(Material.SOUL_SAND);
        ItemMeta meta = placeHolder.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "Prestige Info:");
        placeHolder.setItemMeta(meta);
        return placeHolder;
    }
    public ItemStack placeHolder(){
        ItemStack placeHolder = new ItemStack(Material.SOUL_SAND);
        ItemMeta meta = placeHolder.getItemMeta();
        meta.setDisplayName(ChatColor.WHITE + " ");
        placeHolder.setItemMeta(meta);
        return placeHolder;
    }
    public ItemStack blankSpace(){
        ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE,1,(byte) 15);
        ItemMeta meta = blank.getItemMeta();
        meta.setDisplayName(" ");
        blank.setItemMeta(meta);
        return blank;
    }
    public static ArrayList<ItemStack> tierStacks(ItemStack stack, String itemName, Tier tier, int tierNumber){
        String className1 = tier.getTier1();
        String className2 = tier.getTier2();
        String className3 = tier.getTier3();
        String status1 = ChatColor.AQUA + "Unlocked by default.";
        String status2 = ChatColor.AQUA + "Purchase: " + ChatColor.GOLD + "400" + ChatColor.GOLD + " crystals";
        String status3 = ChatColor.AQUA + "Purchase: " + ChatColor.GOLD + "1200" + ChatColor.GOLD + " crystals";
        String xpRequired2 = ChatColor.DARK_AQUA + "Orbs Required: 650";
        String xpRequired3 = ChatColor.DARK_AQUA + "Orbs Required: 1300";
        ChatColor color = null;
        for(ChatColor colors : ChatColor.values()){
            if(itemName.startsWith(colors.toString())){
                color = colors;
            }
        }
        switch(tierNumber){
            case(2):
                status2 = ChatColor.DARK_AQUA + "Purchased: " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + "400";
                xpRequired2 = "";
                break;
            case(3):
                status2 = ChatColor.DARK_AQUA + "Purchased: " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + "400";
                status3 = ChatColor.DARK_AQUA + "Purchased: " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + "1200";
                xpRequired2 = "";
                xpRequired3 = "";
                break;
        }
        ItemStack tierStack1 = stack.clone();
        ItemMeta meta1 = tierStack1.getItemMeta();
        meta1.setDisplayName(color + className1);
        List<String> lore1 = new ArrayList<>();
        lore1.add(status1);
        meta1.setLore(lore1);
        tierStack1.setItemMeta(meta1);

        ItemStack tierStack2 = stack.clone();
        ItemMeta meta2 = tierStack2.getItemMeta();
        meta2.setDisplayName(color + className2);
        List<String> lore2 = new ArrayList<>();
        lore2.add(status2);
        lore2.add(xpRequired2);
        meta2.setLore(lore2);
        tierStack2.setItemMeta(meta2);

        ItemStack tierStack3 = stack.clone();
        ItemMeta meta3 = tierStack3.getItemMeta();
        meta3.setDisplayName(color + className3);
        List<String> lore3 = new ArrayList<>();
        lore3.add(status3);
        lore3.add(xpRequired3);
        meta3.setLore(lore3);
        tierStack3.setItemMeta(meta3);

        ArrayList<ItemStack> stacks = new ArrayList<>();
        stacks.add(tierStack1);
        stacks.add(tierStack2);
        stacks.add(tierStack3);
        return stacks;
    }
    public ItemStack experienceStack(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack experienceStack = new ItemStack(Material.EXP_BOTTLE);
        ItemMeta meta = experienceStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA + "Orbs:");
        List<String> lore = new ArrayList<>();
        lore.add(user.getExperience() + "");
        meta.setLore(lore);
        experienceStack.setItemMeta(meta);
        return experienceStack;
    }
    public ItemStack balanceStack(Player player){
        User user = main.getUser(player.getUniqueId());
        ItemStack balanceStack = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta meta = balanceStack.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_AQUA + "Crystals:");
        List<String> lore = new ArrayList<>();
        lore.add(user.getBalance() + "");
        meta.setLore(lore);
        balanceStack.setItemMeta(meta);
        return balanceStack;
    }
}
