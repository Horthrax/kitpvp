package me.extendedhorizons.kitverse.blacksmiths;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by horthrax on 6/28/2015.
 */
public class SpawnBlacksmith extends Blacksmith{
    World world = Bukkit.getWorld("world");
    Location location = new Location(world,52.5,13,66.5,180,0);
    public SpawnBlacksmith(Player player){
        ItemStack anvil = new ItemStack(Material.ANVIL);
        this.setOwner(player);
        this.setName(ChatColor.BLACK.toString() + ChatColor.BOLD + "Blacksmith");
        this.setHeldItem(anvil);
        this.setWorld(world);
        this.setLocation(location);
        this.setYaw((byte) 128.9);
        this.loadBlacksmith(this,player);
        Blacksmith.blackSmiths.add(this);
    }
}
