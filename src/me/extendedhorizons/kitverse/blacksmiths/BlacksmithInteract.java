package me.extendedhorizons.kitverse.blacksmiths;

import me.extendedhorizons.kitverse.Main;
import me.extendedhorizons.kitverse.classes.Tier;
import me.extendedhorizons.kitverse.user.User;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by horthrax on 6/28/2015.
 */
public class BlacksmithInteract implements Listener {
    Main main = Main.getInstance();
    EconomyHandler economyHandler = new EconomyHandler();

    @EventHandler
    public void toggleClickEvent(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            ItemStack item = event.getCurrentItem();
            if (item != null && !item.getType().equals(Material.AIR) && item.hasItemMeta() && !item.getItemMeta().getDisplayName().isEmpty()) {
                if (player.getOpenInventory().getTitle().contains("Blacksmith")) {
                    String itemName = item.getItemMeta().getDisplayName();
                    String fixedName = ChatColor.stripColor(itemName.toLowerCase().replace(" ", "_"));
                    if (Tier.isValid(fixedName)) {
                        Tier tier = Tier.getTierFromString(fixedName);
                        if (tier != null) {
                            int tierNumber = main.getUser(player.getUniqueId()).getTier(tier);
                            BlacksmithInventory.openTierGui(player, itemName, tier, tierNumber, item);
                        }
                    }
                }
                for (Tier tiers : Tier.values()) {
                    String correctedTierName = tiers.getName().replaceAll("_", " ");
                    if (ChatColor.stripColor(player.getOpenInventory().getTitle()).toLowerCase().contains(correctedTierName)) {
                        String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
                        User user = main.getUser(player.getUniqueId());

                        int orbs = user.getExperience();
                        int crystals = user.getBalance();

                        int itemsTierLevel = Tier.getTierLevel(tiers, itemName);
                        int tierNumber = main.getUser(player.getUniqueId()).getTier(tiers);

                        int crystals2 = 400; //240/4
                        int crystals3 = 1200; //460/6
                        int xpRequired2 = 650;
                        int xpRequired3 = 1300;

                        switch(itemsTierLevel){
                            case(2):
                                if(tierNumber == 1) {
                                    if(orbs >= xpRequired2) {
                                        if (crystals >= crystals2){
                                            upgradeTier(player.getUniqueId(), tiers, itemsTierLevel, crystals2);
                                            player.closeInventory();
                                        }else {
                                            player.sendMessage(ChatColor.RED + "You don't have enough crystals to purchase this.");
                                        }
                                    }else{
                                        player.sendMessage(ChatColor.RED + "You don't have enough orbs to purchase this.");
                                    }
                                }else if(tierNumber >= 2){
                                    player.sendMessage(ChatColor.AQUA + "You have already unlocked this tier.");
                                }
                                break;
                            case(3):
                                if(tierNumber == 2) {
                                    if(orbs >= xpRequired3) {
                                        if (crystals >= crystals3){
                                            upgradeTier(player.getUniqueId(), tiers, itemsTierLevel, crystals3);
                                            player.closeInventory();
                                        }else {
                                            player.sendMessage(ChatColor.RED + "You don't have enough crystals to purchase this.");
                                        }
                                    }else{
                                        player.sendMessage(ChatColor.RED + "You don't have enough orbs to purchase this.");
                                    }
                                    break;
                                }else if(tierNumber == 1){
                                    player.sendMessage(ChatColor.RED + "You need to unlock the previous tiers to purchase this.");
                                }else if(tierNumber == 3){
                                    player.sendMessage(ChatColor.AQUA + "You have already unlocked this tier.");
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
    public void upgradeTier(UUID uuid, Tier tier, int tierLevel, int crytalsAmount){
        User user = main.getUser(uuid);

        StringBuilder builder = new StringBuilder();
        String sentence = tier.getName().replaceAll("_"," ");
        for (String word : sentence.split(" ")){
            builder.append(StringUtils.capitalize(word)).append(" ");
        }
        String correctedTierName = builder.toString();

        user.getPlayer().sendMessage(ChatColor.AQUA + "You upgraded your " + ChatColor.DARK_AQUA + correctedTierName + ChatColor.AQUA + "to " + ChatColor.DARK_AQUA + tierLevel + ChatColor.AQUA + ".");
        economyHandler.updateEconomy(EconomyHandler.EconomyType.CRYSTALS, EconomyHandler.EconomyCause.PURCHASE, uuid, null, crytalsAmount);
        user.setTier(tier,tierLevel);
    }
}
