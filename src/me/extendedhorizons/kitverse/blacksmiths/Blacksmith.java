package me.extendedhorizons.kitverse.blacksmiths;

import net.minecraft.server.v1_12_R1.*;
import org.bukkit.*;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by horthrax on 6/28/2015.
 */
public class Blacksmith {
    public static List<Blacksmith> blackSmiths = new ArrayList<>();

    Player owner;
    EntityVillager villager;
    String name;
    World world;
    Location location;
    org.bukkit.inventory.ItemStack heldItem;
    byte yaw = 0;
    byte pitch = 0;

    public void loadBlacksmiths(Player player) {
        SpawnBlacksmith spawnBlacksmith = new SpawnBlacksmith(player);
    }

    public void clearBlacksmith(Player player) {
        for (Blacksmith blacksmith : blackSmiths) {
            if (blacksmith.getOwner().equals(player)) {
                PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                try {
                    connection.sendPacket(new PacketPlayOutEntityDestroy(blacksmith.getVillager().getBukkitEntity().getEntityId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void clearAllBlacksmiths(Player player) {
        for (Blacksmith blacksmith : blackSmiths) {
            PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
            try {
                connection.sendPacket(new PacketPlayOutEntityDestroy(blacksmith.getVillager().getBukkitEntity().getEntityId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadBlacksmith(Blacksmith blacksmith, Player player) {
        WorldServer nmsWorld = ((CraftWorld) blacksmith.getWorld()).getHandle();

        EntityVillager villager = new EntityVillager(nmsWorld);
        villager.setLocation(blacksmith.getLocation().getX(), blacksmith.getLocation().getY(), blacksmith.getLocation().getZ(), 180F, 0F);
        villager.setCustomName(blacksmith.getName());
        villager.setCustomNameVisible(true);
        Villager villager1 = (Villager) villager.getBukkitEntity();
        villager1.setCustomNameVisible(true);
        villager1.setProfession(Villager.Profession.BLACKSMITH);
        villager.spawnIn(nmsWorld);
        this.villager = villager;

        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        try {
            connection.sendPacket(new PacketPlayOutSpawnEntityLiving(villager));
            connection.sendPacket(new PacketPlayOutEntityHeadRotation(villager, blacksmith.getYaw()));
            connection.sendPacket(new PacketPlayOutEntity.PacketPlayOutEntityLook(blacksmith.getVillager().getBukkitEntity().getEntityId(), blacksmith.getYaw(), blacksmith.getPitch(), true));
            connection.sendPacket(new PacketPlayOutEntityEquipment(villager.getBukkitEntity().getEntityId(),EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(blacksmith.getHeldItem())));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setHeldItem(ItemStack heldItem) {
        this.heldItem = heldItem;
    }

    public void setYaw(byte yaw) {
        this.yaw = yaw;
    }

    public void setPitch(byte pitch) {
        this.pitch = pitch;
    }

    public void setOwner(Player player) {
        this.owner = player;
    }

    //Getters
    public String getName() {
        return name;
    }

    public World getWorld() {
        return world;
    }

    public Location getLocation() {
        return location;
    }

    public ItemStack getHeldItem() {
        return heldItem;
    }

    public EntityVillager getVillager() {
        return villager;
    }

    public Player getOwner() {
        return owner;
    }

    public byte getYaw() {
        return yaw;
    }

    public byte getPitch() {
        return pitch;
    }
}
