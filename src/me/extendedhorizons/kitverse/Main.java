package me.extendedhorizons.kitverse;

import me.extendedhorizons.kitverse.blacksmiths.Blacksmith;
import me.extendedhorizons.kitverse.blacksmiths.BlacksmithInteract;
import me.extendedhorizons.kitverse.classes.Kit;
import me.extendedhorizons.kitverse.classes.SelectKit;
import me.extendedhorizons.kitverse.classes.Selectors;
import me.extendedhorizons.kitverse.classes.abilities.AbilityTriggers;
import me.extendedhorizons.kitverse.commands.*;
import me.extendedhorizons.kitverse.cosmetics.Leaves;
import me.extendedhorizons.kitverse.cosmetics.chattab.Announcements;
import me.extendedhorizons.kitverse.cosmetics.chattab.ChatFormatting;
import me.extendedhorizons.kitverse.cosmetics.chattab.staffchat.StaffChat;
import me.extendedhorizons.kitverse.cosmetics.holograms.Holograms;
import me.extendedhorizons.kitverse.database.Database;
import me.extendedhorizons.kitverse.inventories.BrewerGui;
import me.extendedhorizons.kitverse.inventories.PlayerStats;
import me.extendedhorizons.kitverse.inventories.SettingsGui;
import me.extendedhorizons.kitverse.shrines.Shrine;
import me.extendedhorizons.kitverse.shrines.ShrineInteract;
import me.extendedhorizons.kitverse.user.User;
import me.extendedhorizons.kitverse.util.Locations;
import me.extendedhorizons.kitverse.util.RespawnHandler;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@SuppressWarnings("unused")
public class Main extends JavaPlugin {
    public static Main instance;
    public Holograms holograms;
    public Database database;
    public Locations locations;
    public Leaves leaves;
    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Shrine> shrines = new ArrayList<>();
    public Kit kits;
    public Blacksmith blacksmith;
    public Selectors selectors;

    private ArrayList<Object> frameUpdates = new ArrayList<>();
    public ArrayList<Arrow> arrows = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        //pok
        clearEntities();
        this.database = new Database();
        this.database.initialize();
        this.holograms = new Holograms();
        this.leaves = new Leaves();
        this.kits = new Kit();
        this.blacksmith = new Blacksmith();
        this.selectors = new Selectors();
        this.selectors.loadAllSelectors();
        World w = Bukkit.getWorld("world");
        w.setStorm(false);
        w.setTime(0);
        getCommand("setrank").setExecutor(new RanksExecutor());
        getCommand("clearchat").setExecutor(new ClearChatExecutor());

        getCommand("creative").setExecutor(new GamemodeExecutor());
        getCommand("survival").setExecutor(new GamemodeExecutor());
        getCommand("spectator").setExecutor(new GamemodeExecutor());
        getCommand("adventure").setExecutor(new GamemodeExecutor());

        getCommand("settier").setExecutor(new TierExecutor());
        getCommand("resettiers").setExecutor(new TierExecutor());

        getCommand("setkills").setExecutor(new KDRExecutor());
        getCommand("setdeaths").setExecutor(new KDRExecutor());

        getCommand("balance").setExecutor(new BalanceExecutor());

        loadEvents();
        loadShrines();
        loadUpdates();
        frameUpdate();
        particles();
        Announcements announcements = new Announcements();
    }

    private void loadUpdates() {
        //registerFrameUpdates(new Orb(locations.orb));
    }



    private void loadShrines() {
        World world = Bukkit.getWorld("world");
        Location redLocation = new Location(world, -10.5, 38.5, 131.5);
        Location blueLocation = new Location(world, -22.5, 34.5, -13.5);
        Location greenLocation = new Location(world, 165.5, 40.5, -3.5);

        Shrine blueShrine = new Shrine(blueLocation, "Blue", ChatColor.BLUE, PotionEffectType.SPEED, 20*60*3, 0);
        Shrine redShrine = new Shrine(redLocation, "Red", ChatColor.RED, PotionEffectType.HEALTH_BOOST, 20*60*2, 1);
        Shrine greenShrine = new Shrine(greenLocation, "Green", ChatColor.GREEN, PotionEffectType.DAMAGE_RESISTANCE, 20*60, 1);
    }

    public void onDisable(){
        this.holograms.clearHolograms();
        this.database.closeConnection();
        this.selectors.clearSelectors();
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(getUser(player.getUniqueId()) != null) {
                this.getUser(player.getUniqueId()).updateDatabase();
            }
            this.kits.clearAllKits(player);
            this.blacksmith.clearBlacksmith(player);
            player.kickPlayer(ChatColor.WHITE + "» " + ChatColor.RED + "The server is restarting or reloading, this should only take a few seconds." + ChatColor.WHITE + " «");
        }
        saveConfig();
    }

    public void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }

    public void registerFrameUpdates(Object... objects){
        for(Object object : objects){
            this.frameUpdates.add(object);
        }
    }

    public User getUser(UUID uuid){
        for(User user : this.users){
            if(user.getUUID().equals(uuid)){
                return user;
            }
        }
        return null;
    }

    public Collection<? extends User> getUsers(){
        return this.users;
    }

    public User getUser(String userName){
        for(User user : this.users){
            if(user.getPlayer().getName().equals(userName)){
                return user;
            }
        }
        return null;
    }

    public static Main getInstance() {
        return instance;
    }

    public void particles(){
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Entity entities : Bukkit.getWorld("world").getEntities()) {
                    if (entities instanceof Arrow) {
                        Arrow arrow = (Arrow) entities;
                        if (arrows.contains(arrow)) {
                            spawnParticle(arrow.getLocation(), EnumParticle.VILLAGER_HAPPY, true, 0, 0, 0, 0, 1);
                        } else {
                            arrows.add(arrow);
                        }
                    }
                }
            }

        }.runTaskTimer(this, 1,1);

    }

    public void spawnParticle(Location location, EnumParticle particle, boolean longRange, int xOffs, int yOffs, int zOffs, int speed, int size){
        PacketPlayOutWorldParticles particlesPacket = new PacketPlayOutWorldParticles(particle,longRange,(float)location.getX(),(float)(location.getY()), (float)location.getZ(), xOffs,yOffs,zOffs,speed,size);
        for(Player players : Bukkit.getOnlinePlayers()){
            PlayerConnection connection = ((CraftPlayer) players).getHandle().playerConnection;
            connection.sendPacket(particlesPacket);
        }
    }

    public void loadEvents(){
        registerEvents(this, new Events());
        registerEvents(this, new BlacksmithInteract());
        registerEvents(this, new PlayerStats());
        registerEvents(this, new ChatFormatting());
        registerEvents(this, new SelectKit());
        registerEvents(this, new AbilityTriggers());
        registerEvents(this, new SettingsGui());
        registerEvents(this, new RespawnHandler());
        registerEvents(this, new BrewerGui());
        registerEvents(this, new StaffChat());
        registerEvents(this, new ShrineInteract());
    }

    public void clearEntities() {
        World world = Bukkit.getWorld("world");
        for (Entity en : world.getEntities()) {
            en.remove();
        }
    }

    public Database getDB(){
        return this.database;
    }

    public Selectors getSelectors() {
        return this.selectors;
    }

    private void frameUpdate(){
        new BukkitRunnable(){
            int tick = 0;
            @Override
            public void run(){
                for(Object object : frameUpdates){
                    try {
                        Method method = object.getClass().getDeclaredMethod("onUpdate");
                        if(method != null) {
                            method.setAccessible(true);
                            method.invoke(object);
                            tick++;
                        }
                    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.runTaskTimer(this,0,1);
    }
}